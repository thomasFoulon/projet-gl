/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.IMAProgram;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BOV;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.ERROR;
import fr.ensimag.ima.pseudocode.instructions.TSTO;
import fr.ensimag.ima.pseudocode.instructions.WNL;
import fr.ensimag.ima.pseudocode.instructions.WSTR;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Class to administrate the errors which can occurs during the execution.
 * @author gl38
 * @date 15/01/2020
 */
public class ErrorManager {
    
    public static final Label DIV_BY_ZERO_INT = new Label("div_by_zero");
    public static final Label FLOAT_OVERFLOW = new Label("float_overflow");
    public static final Label INVALID_TYPE_CONVERSION = new Label("invalid_type_conversion");
    public static final Label INVALID_INPUT = new Label("invalid_input");
    public static final Label STACK_OVERFLOW = new Label("stack_overflow");
    public static final Label HEAP_OVERFLOW = new Label("heap_overflow");
    public static final Label NULL_DEREFERENCE = new Label("null_dereference");
    
    private Map<Label, List<Instruction>> map;

    public ErrorManager(DecacCompiler compiler) {
        this.map = new HashMap<>();
    }
    
    /**
     * Add a new error handler to the program.
     * @param label the label where the handler has to be written.
     */
    private void newErrorHandler(Label label, String errorMessage){
        assert(label != null);
        if (!this.map.containsKey(label)) {
            this.map.put(label, new LinkedList<>());
            this.addInstruction(label, new WSTR("error : " + errorMessage));
        }
    }
    
    /**
     * Add an instruction to a handler.
     * @param label the label of the handler
     * @param inst the instruction to add to the handler
     */
    private void addInstruction(Label label, Instruction inst){
        assert(label != null);
        assert(inst != null);
        assert(this.map.get(label) != null);
        this.map.get(label).add(inst);
    }
    
    /**
     * Generate the assembly code of a handler.
     * @param compiler the compiler
     * @param label the label of the handler
     */
    private void generateErrorHandler(DecacCompiler compiler, Label label){
        assert(compiler != null);
        assert(label != null);
        assert(this.map.get(label) != null);
        compiler.addLabel(label);
        for(Instruction inst : this.map.get(label)){
            compiler.addInstruction(inst);
        }
        compiler.addInstruction(new WNL());
        compiler.addInstruction(new ERROR());
    }
    
    /**
     * Generate the assembly code of each handler.
     * @param compiler the compiler
     */
    public void generateErrorHandlers(DecacCompiler compiler){
        assert(compiler != null);
        compiler.addComment(" --------- Error handlers");
        for(Label label : this.map.keySet()){
            generateErrorHandler(compiler, label);
        }
    }
    
    public boolean needCheck(DecacCompiler compiler) {
        return !compiler.getCompilerOptions().getNoCheck();
    }
    
    /**
     * Generate the code for an overflow check that will
     * print "error : overflow on arithmetic operation with float" if necessary
     * @param compiler the compiler
     */
    public void addErrorOverflowFloat(DecacCompiler compiler) {
        if (this.needCheck(compiler)) {
            this.newErrorHandler(FLOAT_OVERFLOW, "overflow on arithmetic operation with float");
            compiler.addInstruction(new BOV(FLOAT_OVERFLOW));
        }
    }
    
    /**
     * Generate the code for an overflow check that will
     * print "error : division by zero" if necessary
     * @param compiler the compiler
     */
    public void addErrorDivZeroInt(DecacCompiler compiler) {
        this.newErrorHandler(DIV_BY_ZERO_INT, "division by zero");
        compiler.addInstruction(new BOV(DIV_BY_ZERO_INT));
    }
    
    
    /**
     * Generate the code for an overflow check that will
     * print "error : invalid type conversion" if necessary
     * @param compiler the compiler
     */
    public void addErrorInvalidTypeConversion(DecacCompiler compiler) {
        this.newErrorHandler(INVALID_TYPE_CONVERSION, "invalid type conversion");
        compiler.addInstruction(new BOV(INVALID_TYPE_CONVERSION));
    }
    
    
    /**
     * Generate the code for an overflow check that will
     * print "error : invalid input" if necessary
     * @param compiler the compiler
     */
    public void addErrorInvalidInput(DecacCompiler compiler) {
        this.newErrorHandler(INVALID_INPUT, "invalid input");
        compiler.addInstruction(new BOV(INVALID_INPUT));
    }
    
    /**
     * Generate the code for an overflow check of the stack that will
     * print "error : stack overflow" if necessary
     * @param compiler the compiler
     * @param program the program where will be added the check
     * @param maxStackSize the maximal size of the stack in the given program
     *                      (it is used to create the instruction TSTO for the check)
     */
    public void addErrorStackOverflow(DecacCompiler compiler, IMAProgram program, int maxStackSize) {
        if (this.needCheck(compiler)) {
            this.newErrorHandler(STACK_OVERFLOW, "stack overflow");
            program.addFirst(new BOV(ErrorManager.STACK_OVERFLOW));
            program.addFirst(new TSTO(maxStackSize), "maximal size of stack");
        }
    }
    
    /**
     * Generate the code for an overflow check that will
     * print "error : impossible allocation, heap full" if necessary
     * @param compiler the compiler
     */
    public void addErrorHeapOverflow(DecacCompiler compiler) {
        if (this.needCheck(compiler)) {
            this.newErrorHandler(HEAP_OVERFLOW, "impossible allocation, heap full");
            compiler.addInstruction(new BOV(HEAP_OVERFLOW));
        }
    }
    
    /**
     * Generate the code for a null dereference check that will
     * print "error : null dereference" if necessary
     * @param compiler the compiler
     * @param registerNumber the register were the address of the object is stored
     */
    public void addErrorNullDereference(DecacCompiler compiler, int registerNumber){
        if (this.needCheck(compiler)) {
            this.newErrorHandler(NULL_DEREFERENCE, "null dereference");
            compiler.addInstruction(new CMP(new NullOperand(), compiler.getR(registerNumber)));
            compiler.addInstruction(new BEQ(ErrorManager.NULL_DEREFERENCE));
        }
    }
    
    /**
     * Generate the code to exit the program with an error and printing
     * "error : ended method methodName without return
     * @param compiler the compiler
     * @param methodName the name of the method
     */
    public void addErrorNoReturn(DecacCompiler compiler, String methodName){
        compiler.addInstruction(new WSTR("error : ended method " + methodName + " without return"));
        compiler.addInstruction(new WNL());
        compiler.addInstruction(new ERROR());
    }
}
