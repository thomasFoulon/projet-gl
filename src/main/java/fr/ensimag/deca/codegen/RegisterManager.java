/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;

/**
 * Class to manage Registers
 * @author gl38
 * @date 14/01/2020
 */
public class RegisterManager {
    
    /**
     * Generate assembly code to save the register in the compiler stack
     * @param compiler the compiler
     * @param numRegister number of the register to save
     */
    public static void saveRegister(DecacCompiler compiler, int numRegister) {
        compiler.getStackManager().useTmpRegister();
        compiler.addInstruction(new PUSH(compiler.getR(numRegister)));
    }

    /**
     * Generate assembly code to add instructions to save the content
     * of register numRegister in R0 and put the stacked value in register numRegister
     * @param compiler the compiler
     * @param numRegister number of the register to restore
     */
    public static void restoreRegister(DecacCompiler compiler, int numRegister) {
        compiler.getStackManager().freeTmpRegister();
        GPRegister register = compiler.getR(numRegister);
        compiler.addInstruction(new LOAD(register, Register.R0));
        compiler.addInstruction(new POP(register), "restore");        
    }

    /**
     * Obtain the number of the next register to use
     * it is equal to numRegister if there is no more register available
     * (ie numRegister == max number of register) and it generate the code to
     * stack the value of the register numRegister
     * otherwise it returns numRegister + 1
     * @param compiler the compiler
     * @param numRegister the current register used
     * @return the number of the next usable register
     *          it is equal to numRegister if there is no more register available
     */
    public static int getNextRegister(DecacCompiler compiler, int numRegister) {
        if (compiler.getCompilerOptions().getRegisters() == numRegister) {
            RegisterManager.saveRegister(compiler, numRegister);
            return numRegister;
        } else {
            return numRegister + 1;
        }
    }

    /**
     * Obtain the number of the register where the expression result is stored
     * If numRegister == numRegRight : generate the code to restore its result
     * from the stack and place the current value of numRegRight in R0
     * @param compiler the compiler
     * @param numRegister the register where the expression should have its results
     * @param numRegRight the register where the last expression put its results
     * @return 0 if a register was stacked
     *          numRegRight otherwise
     */
    public static int getStoredRegister(DecacCompiler compiler, int numRegister, int numRegRight) {
        if (numRegRight == numRegister) {
            RegisterManager.restoreRegister(compiler, numRegRight);
            return 0;
        } else {
            return numRegRight;
        }
    }
}
