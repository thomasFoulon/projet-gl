/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.ima.pseudocode.Label;
import java.util.HashSet;
import java.util.Set;

/**
 * Class to administrate labels.
 * @author gl38
 * @date 15/01/2020
 */
public class LabelManager {
    
    private int nbBooleanDeclVar;
    private int nbBooleanExp;
    private int nbIf;
    private int nbInstanceOf;
    private int nbCast;
    private int nbWhile;
    private Set<Label> errorLabels;

    public LabelManager() {
        this.nbBooleanDeclVar = 0;
        this.nbBooleanExp = 0;
        this.nbIf = 0;
        this.nbWhile = 0;
        this.nbInstanceOf = 0;
        this.nbCast = 0;
        this.errorLabels = new HashSet<>();
    }
    
    /**
     * Creates and returns a new if structure labels.
     * @return an array containing the labels required for a if structure in assembly
     *          position 0 -> L_EndIf
     *          position 1 -> L_Else
     */
    public Label[] newIfLabels(){
        ++nbIf;
        Label[] labels = new Label[2];
        labels[0] = new Label("L_EndIf." + Integer.toString(nbIf));
        labels[1] = new Label("L_Else." + Integer.toString(nbIf));
        return labels;
    }
    
    /**
     * Creates and returns new while structure labels.
     * @return an array containing the labels required for a while structure in assembly
     *          position 0 -> L_WhileStart
     *          position 1 -> L_WhileCond
     */
    public Label[] newWhileLabels(){
        ++nbWhile;
        Label[] labels = new Label[2];
        labels[0] = new Label("L_WhileStart." + Integer.toString(nbWhile));
        labels[1] = new Label("L_WhileCond." + Integer.toString(nbWhile));
        return labels;
    }
    
    /**
     * Creates and returns new instanceOf structure labels.
     * @return an array containing the labels required for a while structure in assembly
     *          position 0 -> L_InstanceOfStart
     *          position 1 -> L_InstanceOfEnd
     *          position 2 -> L_InstanceOfTrue
     *          position 3 -> L_InstanceOfFalse
     */
    public Label[] newInstanceOfLabels(){
        ++nbInstanceOf;
        Label[] labels = new Label[4];
        labels[0] = new Label("L_InstanceOfStart." + Integer.toString(nbInstanceOf));
        labels[1] = new Label("L_InstanceOfEnd." + Integer.toString(nbInstanceOf));
        labels[2] = new Label("L_InstanceOfTrue." + Integer.toString(nbInstanceOf));
        labels[3] = new Label("L_InstanceOfFalse." + Integer.toString(nbInstanceOf));
        return labels;
    }
    
    /**
     * Creates and returns a new label for a valid cast
     * @return  the new label
     */
    public Label newCastOKLabel(){
        ++nbCast;
        return new Label("L_CastOK." + Integer.toString(nbCast));
    }
    
    /**
     * Creates and returns a new label for the end of a boolean expression.
     * @return  the new label
     */
    public Label newEndBooleanExpLabel(){
        ++nbBooleanExp;
        return new Label("L_EndBoolExp." + Integer.toString(nbBooleanExp));
    }
    
    /**
     * Creates and returns new variable definition labels.
     * @return an array containing the labels required for a boolean variable definition in assembly
     *          position 0 -> L_DefTrue
     *          position 1 -> L_DefFalse
     *          position 2 -> L_EndDefBool
     */
    public Label[] newBooleanDeclVarLabels(){
        ++nbBooleanDeclVar;
        Label[] labels = new Label[3];
        labels[0] = new Label("L_DefTrue." + Integer.toString(nbBooleanDeclVar));
        labels[1] = new Label("L_DefFalse." + Integer.toString(nbBooleanDeclVar));
        labels[2] = new Label("L_EndDefBool." + Integer.toString(nbBooleanDeclVar));
        return labels;
    }
    
}
