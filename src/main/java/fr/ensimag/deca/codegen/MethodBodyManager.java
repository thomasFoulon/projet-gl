/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.AbstractLine;
import fr.ensimag.ima.pseudocode.IMAProgram;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Line;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.RTS;

/**
 * Class that manage a method body with its own program and stack manager
 * @author gaisnea
 */
public class MethodBodyManager {
    private IMAProgram program;
    private StackManager stack;
    private Label nameOfMethod;
    private boolean returnIsVoid;
    
    public MethodBodyManager(Label nameOfMethod, boolean returnIsVoid) {
        this.program = new IMAProgram();
        this.stack = new StackManager(program);
        this.nameOfMethod = nameOfMethod;
        this.returnIsVoid = returnIsVoid;
    }
    
    public IMAProgram getProgram() {
        return this.program;
    }
    
    public StackManager getStackManager() {
        return this.stack;
    }
    
    public Label getNameOfMethod(){
        return this.nameOfMethod;
    }
    
    /**
     * Generate the code to save the needed register, manage the stack
     * for this method and return to the caller
     * If the method's return is not a void, it generate an error if no return
     * was written
     */
    public void endMethodBody(DecacCompiler compiler) {
        this.addInstructionRegisterSave();
        this.getStackManager().codeGenStackInit(compiler);
        this.program.addFirst(new Line(nameOfMethod));
        if (!this.returnIsVoid) {
            compiler.addErrorNoReturn(nameOfMethod.toString());
        }
        
        this.program.addLabel(new Label("fin." + this.nameOfMethod));
        this.addInstructionRegisterRestore();
        this.program.addInstruction(new RTS());
    }
    
    /**
     * Generate code to save all register used during the method call
     * (register from R2 to R15, R0 and R1 are not saved)
     */
    private void addInstructionRegisterSave() {
        this.program.addFirst(new Line("End save registers"));
        for (int i = 2; i <= this.getStackManager().getNumHigherRegister(); i++) {
            this.stack.addSavedRegister(i);
        }
        this.program.addFirst(new Line("Save registers"));
    }
    
    /**
     * Generate code to restore all register used during the method call
     * (register from R2 to R15, R0 and R1 are not restored)
     */
    private void addInstructionRegisterRestore() {
        this.program.addComment("Restore registers");
        for (int i = 2; i <= this.getStackManager().getNumHigherRegister(); i++) {
            this.program.addInstruction(new POP(Register.getR(i)));
        }
        this.program.addComment("End restore registers");
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#add(fr.ensimag.ima.pseudocode.AbstractLine)
     */
    public void add(AbstractLine line) {
        this.program.add(line);
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#addComment(java.lang.String)
     */
    public void addComment(String comment) {
        this.program.addComment(comment);
    }
    
    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addLabel(fr.ensimag.ima.pseudocode.Label)
     */
    public void addLabel(Label label) {
        this.program.addLabel(label);
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag.ima.pseudocode.Instruction)
     */
    public void addInstruction(Instruction instruction) {
        this.program.addInstruction(instruction);
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag.ima.pseudocode.Instruction,
     * java.lang.String)
     */
    public void addInstruction(Instruction instruction, String comment) {
        this.program.addInstruction(instruction, comment);
    }
}
