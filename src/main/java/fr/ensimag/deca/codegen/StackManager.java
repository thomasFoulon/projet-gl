/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.VariableDefinition;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.IMAProgram;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.ADDSP;
import fr.ensimag.ima.pseudocode.instructions.POP;
import fr.ensimag.ima.pseudocode.instructions.PUSH;
import fr.ensimag.ima.pseudocode.instructions.SUBSP;

/**
 * Class to administrate the stack.
 * @author gl38
 * @date 14/01/2020
 */
public class StackManager {
    
    private int offsetLB;
    private int nbCurrentTmpRegister;
    private int maxTmpRegister;
    private int registerSaved;
    private IMAProgram program;
    private int numHigherRegister;

    public StackManager(IMAProgram prog) {
        this.offsetLB = 1;
        this.nbCurrentTmpRegister = 0;
        this.maxTmpRegister = 0;
        this.registerSaved = 0;
        this.program = prog;
        this.numHigherRegister = 0;
    }
    
    public int getNumHigherRegister() {
        return this.numHigherRegister;
    }
    
    public void setNumHigherRegister(int numReg) {
        if (this.numHigherRegister < numReg) {
            this.numHigherRegister = numReg;
        }
    }
    
    /**
     * Declare a variable by setting its address (using Local Base)
     * @param def the definition of the variable where we put its address
     */
    public void declareVariable(VariableDefinition def){
        def.setOperand(new RegisterOffset(offsetLB, Register.LB));
        offsetLB++;
    }

    /**
     * Declare a class by adding its method table
     * @param def
     * @return the offset in the stack
     */
    public int declareClass (ClassDefinition def){
        int offsetOfClass = offsetLB;
        def.setOperand(new RegisterOffset(offsetOfClass, Register.GB));
        int methods = def.getNumberOfMethods();
        // number of method + adress of the super class table method
        offsetLB = offsetOfClass + methods + 1;
        return offsetOfClass;
    }    
    
    /**
     * Called when the stack is used to store a register
     * (when all register are used)
     */
    public void useTmpRegister() {
        this.nbCurrentTmpRegister++;
        if (this.nbCurrentTmpRegister > this.maxTmpRegister) {
            this.maxTmpRegister = this.nbCurrentTmpRegister;
        }
    }
    
    /**
     * Called when the stacked register is poped out of the stack
     */
    public void freeTmpRegister() {
        this.nbCurrentTmpRegister--;
    }
    
    /**
     * Generate the code to initialize the stack
     * at the beginning of the IMAProgramm of the stack
     * Check for stack overflow
     */
    public void codeGenStackInit(DecacCompiler compiler) {
        int maxStackSize = registerSaved + offsetLB + maxTmpRegister - 1;
        if (maxStackSize > 0) {
            this.program.addFirst(new ADDSP(offsetLB - 1));
            compiler.addErrorStackOverflow(this.program, maxStackSize);
        }
    }

    /**
     * Push the register for function call
     * @param reg the register to stack
     */
    public void pushParamForFunctionCall(GPRegister reg) {
        this.useTmpRegister();
        this.program.addInstruction(new PUSH(reg));
    }
    
    /**
     * Pop the register stacked for function call
     * @param reg the register where the stacked value will be restored
     */
    public void popParamForFunctionCall(GPRegister reg) {
        this.freeTmpRegister();
        this.program.addInstruction(new POP(reg));
    }
    
    /**
     * Generate code to add enough room in the stack to store the parameters
     * @param numParameters number of parameters to store in the stack
     */
    public void addStackForFunctionCall(int numParameters){
        this.maxTmpRegister = this.maxTmpRegister + numParameters;
        this.program.addInstruction(new ADDSP(numParameters));
    }
    
    /**
     * Generate code to unstack all parameters
     * This function doesn't store the parameters
     * @param numParameters 
     */
    public void popAllParamForFunctionCall(int numParameters){
        this.nbCurrentTmpRegister = this.nbCurrentTmpRegister - numParameters;
        this.program.addInstruction(new SUBSP(numParameters)); 
    }

    /**
     * Called when a register must be saved to the stack
     * @param numRegister the number of the register that will be pushed on the stack
     */
    public void addSavedRegister(int numRegister) {
        this.registerSaved++;
        this.program.addFirst(new PUSH(Register.getR(numRegister)));
    }
}
