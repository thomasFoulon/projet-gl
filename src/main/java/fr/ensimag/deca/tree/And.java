package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Label;


/**
 * And operation (&&).
 * @author gl38
 * @date 01/01/2020
 */
public class And extends AbstractOpBool {

    public And(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "&&";
    }

    @Override
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        assert(compiler != null);
        assert(label != null);
        if(branchIfTrue){
            Label end = compiler.getLabelManager().newEndBooleanExpLabel();
            this.getLeftOperand().codeGenCondition(compiler, false, end);
            this.getRightOperand().codeGenCondition(compiler, true, label);
            compiler.addLabel(end);
        }
        else{
            this.getLeftOperand().codeGenCondition(compiler, false, label);
            this.getRightOperand().codeGenCondition(compiler, false, label);
        }
    }

}
