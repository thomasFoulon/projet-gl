package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;

/**
 * Boolean expressions (&&, ||).
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractOpBool extends AbstractBinaryExpr {

    public AbstractOpBool(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        Type tLeft = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type tRight = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!(tLeft.isBoolean())) {
            throw new ContextualError("Left operand must be of type (boolean).", this.getLeftOperand().getLocation());
        }
        if (!(tRight.isBoolean())) {
            throw new ContextualError("Right operand must be of type (boolean).", this.getRightOperand().getLocation());
        }
        this.setType(compiler.getType("boolean"));
        return this.getType();
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        Label[] definitionLabels = compiler.getLabelManager().newBooleanDeclVarLabels();
        this.codeGenCondition(compiler, true, definitionLabels[0]);
        this.codeGenCondition(compiler, false, definitionLabels[1]);
        compiler.addLabel(definitionLabels[0]);
        compiler.addInstruction(new LOAD(1, compiler.getR(numRegister)));
        compiler.addInstruction(new BRA(definitionLabels[2]));
        compiler.addLabel(definitionLabels[1]);
        compiler.addInstruction(new LOAD(0, compiler.getR(numRegister)));
        compiler.addLabel(definitionLabels[2]);
    }
    
    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {
        // never called
    }

}
