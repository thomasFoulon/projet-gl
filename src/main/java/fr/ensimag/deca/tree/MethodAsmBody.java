/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.InlinePortion;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.RTS;

import java.io.PrintStream;

/**
 * ASM code in a method body (asm()).
 * @author gl38
 * @date 17/01/2020
 */
public class MethodAsmBody extends AbstractMethodBody{

    final private String code;
    final private Location location;

    public MethodAsmBody(String code, Location location) {
        this.code = code;
        this.location = location;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        String codeToDecompile = code;
        codeToDecompile = codeToDecompile.replace("\\", "\\\\");
        codeToDecompile = codeToDecompile.replace("\"", "\\\"");
        s.println("asm(\"" + codeToDecompile + "\");");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // nothing to do
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // nothing to do
    }

    @Override
    public void verifyMethodBody(DecacCompiler compiler, ClassDefinition classDef, EnvironmentExp paramsEnv, Type returnT) throws ContextualError {
        // nothing to do
    }

    @Override
    protected void codeGenMethodBody(DecacCompiler compiler, Label labelOfMethod, boolean returnIsVoid) {
        compiler.addLabel(labelOfMethod);
        compiler.addComment("Asm method:");
        compiler.add(new InlinePortion(code));
    }
    
}
