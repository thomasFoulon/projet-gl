package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.MethodBodyManager;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Return statement.
 * @author gl38
 * @date 01/01/2020
 */
public class Return extends AbstractInst {

    private AbstractExpr returnExpr;

    public AbstractExpr getReturnExpr() {
        return returnExpr;
    }

    public Return(AbstractExpr returnExpr) {
        Validate.notNull(returnExpr);
        this.returnExpr = returnExpr;
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        assert(compiler != null);
        MethodBodyManager methodBody = compiler.getMethodBodyManager();
        assert(methodBody != null);
        Label nameOfMethod = methodBody.getNameOfMethod();
        compiler.addComment("Return instruction");
        returnExpr.codeGenExpr(compiler, 2);
        compiler.addInstruction(new LOAD(compiler.getR(2), Register.R0));
        compiler.addInstruction(new BRA(new Label("fin."+nameOfMethod)));
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        assert(compiler != null);
        if(returnType.isVoid()){
            throw new ContextualError("A return expression cannot be of type void.", this.returnExpr.getLocation());
        }
        AbstractExpr newExpr = returnExpr.verifyRValue(compiler, localEnv, currentClass, returnType);
        // In case of ConvFloat
        this.returnExpr = newExpr;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("return ");
        getReturnExpr().decompile(s);
        s.print(";");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        returnExpr.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        returnExpr.prettyPrint(s, prefix, true);
    }

}
