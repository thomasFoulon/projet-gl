package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.instructions.REM;

/**
 * Modulo operation (%).
 * @author gl38
 * @date 01/01/2020
 */
public class Modulo extends AbstractOpArith {

    public Modulo(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        Type t1 = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type t2 = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!(t1.isInt())) {
            throw new ContextualError("Left operand must be of type (int).", this.getLeftOperand().getLocation());
        }
        if (!(t2.isInt())) {
            throw new ContextualError("Right operand must be of type (int).", this.getRightOperand().getLocation());
        }
        this.setType(compiler.getType("int"));
        return this.getType();
    }

    @Override
    protected String getOperatorName() {
        return "%";
    }

    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {        
        compiler.addInstruction(new REM(dvalRight, compiler.getR(numRegister)));
        compiler.getErrorManager().addErrorDivZeroInt(compiler);
    }

}
