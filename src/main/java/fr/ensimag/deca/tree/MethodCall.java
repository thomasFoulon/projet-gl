/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.BSR;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import java.io.PrintStream;
import java.util.Iterator;

/**
 * Method call.
 * @author gl38
 * @date 17/01/2020
 */
public class MethodCall extends AbstractExpr {

    private final AbstractExpr object;
    private final AbstractIdentifier method;
    private final ListExpr args;

    public MethodCall(AbstractExpr object, AbstractIdentifier method, ListExpr args) {
        this.object = object;
        this.method = method;
        this.args = args;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type objectType = object.verifyExpr(compiler, localEnv, currentClass);
        if(!objectType.isClass()){
            throw new ContextualError(objectType.getName() + " cannot be dereferenced.", this.getLocation());
        }
        Definition def = compiler.getEnvironmentType().get(objectType.getName());
        ClassDefinition exprClassDef = (ClassDefinition) def;
        MethodDefinition methodDef = method.verifyMethodIdent(exprClassDef.getMembers());
        Iterator<Type> paramTypes = methodDef.getSignature().iterator();
        // Verify the types of the parameters are matching the signature
        for(int i = 0; i < args.size(); ++i){
            AbstractExpr param = args.getList().get(i);
            if(!paramTypes.hasNext()){
                throw new ContextualError("Too many parameters for the method " + method.getName() + ".", this.getLocation());
            }
            AbstractExpr newParam = param.verifyRValue(compiler, localEnv, currentClass, paramTypes.next());
            // In case of ConvFloat
            args.set(i, newParam);
        }
        if(paramTypes.hasNext()){
            throw new ContextualError("Missing parameters for the method " + method.getName() + ".", this.getLocation());
        }
        // Decoration
        this.method.setDefinition(methodDef);
        this.setType(methodDef.getType());
        return methodDef.getType();
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        // Add to the stack, the number of parameters
        int numberOfParameters = this.args.size() + 1; // Including the implicit parameter
        compiler.addComment("Call of method");
        compiler.getStackManager().addStackForFunctionCall(numberOfParameters);
        // Stack the implicit parameter (object)
        this.object.codeGenExpr(compiler, numRegister);
        compiler.addInstruction(new STORE(compiler.getR(numRegister), new RegisterOffset(0, Register.SP)));
        // Stack the rest of params
        int offset = -1;
        for (AbstractExpr e : this.args.getList()){
            e.codeGenExpr(compiler, numRegister);
            compiler.addInstruction(new STORE(compiler.getR(numRegister), new RegisterOffset(offset, Register.SP)));
            offset--;
        }
        // Retreive the implicit parameter
        compiler.addInstruction(new LOAD(new RegisterOffset(0, Register.SP), compiler.getR(numRegister)));
        compiler.addErrorNullDereference(numRegister);
        compiler.addInstruction(new LOAD(new RegisterOffset(0, compiler.getR(numRegister)), compiler.getR(numRegister)));
        compiler.addInstruction(new BSR(new RegisterOffset(this.method.getMethodDefinition().getIndex(), compiler.getR(numRegister)))); //Call method
        compiler.addInstruction(new LOAD(Register.R0, compiler.getR(numRegister))); // In case of return, save result in register 0
        compiler.getStackManager().popAllParamForFunctionCall(numberOfParameters);
        
        compiler.addComment("Call of method ended");
    }
    
    @Override
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label){
        this.codeGenExpr(compiler, 2);
        compiler.addInstruction(new CMP(new ImmediateInteger(1), compiler.getR(2)));
        if (branchIfTrue) {
            compiler.addInstruction(new BEQ(label));
        } else {
            compiler.addInstruction(new BNE(label));
        }
    }


    @Override
    public void decompile(IndentPrintStream s) {
        object.decompile(s);
        s.print(".");
        method.decompile(s);
        s.print("(");
        args.decompile(s);
        s.print(")");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        object.prettyPrint(s, prefix, false);
        method.prettyPrint(s, prefix, false);
        args.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        object.iter(f);
        method.iter(f);
        args.iter(f);
    }

}
