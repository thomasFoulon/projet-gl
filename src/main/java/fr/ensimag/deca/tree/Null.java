/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.NullType;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import java.io.PrintStream;

/**
 * Null expression (null).
 * @author gl38
 * @date 17/01/2020
 */
public class Null extends AbstractExpr {

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type nullType = new NullType(compiler.getSymbol("null"));
        this.setType(nullType);
        return nullType;
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        compiler.addInstruction(new LOAD(this.getDVal(), compiler.getR(numRegister)));
    }

    @Override
    protected DVal getDVal() {
        return new NullOperand();
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("null");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // nothing to do
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // nothing to do
    }

}
