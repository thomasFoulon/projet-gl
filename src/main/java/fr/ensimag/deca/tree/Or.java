package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Label;

/**
 * Or operation (||).
 * @author gl38
 * @date 01/01/2020
 */
public class Or extends AbstractOpBool {

    public Or(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "||";
    }

    @Override
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        new Not(new And(new Not(getLeftOperand()), new Not(getRightOperand()))).codeGenCondition(compiler, branchIfTrue, label);
    }

}
