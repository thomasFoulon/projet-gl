/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.context.TypeDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BSR;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.NEW;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import java.io.PrintStream;

/**
 * New expression (new ...();).
 * @author gl38
 * @date 17/01/2020
 */
public class New extends AbstractExpr {

    private final AbstractIdentifier className;

    public New(AbstractIdentifier className) {
        this.className = className;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        TypeDefinition typeDef = compiler.getEnvironmentType().get(className.getName());
        if(typeDef == null){
            throw new ContextualError("The class " + className.getName() + " doesn't exist.", className.getLocation());
        }
        if(!(typeDef.isClass())){
            throw new ContextualError("Cannot instanciate a non-object type.", this.getLocation());
        }
        // Decorations
        this.className.setDefinition(typeDef);
        this.className.setType(typeDef.getType());
        this.setType(typeDef.getType());
        className.setDefinition(typeDef);
        return this.getType();
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        GPRegister reg = compiler.getR(numRegister);
        compiler.addInstruction(new NEW(
                className.getClassDefinition().getNumberOfFields() + 1
                , reg));
        compiler.addErrorHeapOverflow();
        DAddr addrTableMethod = className.getClassDefinition().getOperand();
        compiler.addInstruction(new LEA(addrTableMethod, Register.R0));
        compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(0, reg)));
        if (className.getClassDefinition().getNumberOfFields() > 0) {
            this.codeGenInit(compiler, this.className.getName().getName(), reg);
        }
    }
    
    private void codeGenInit(DecacCompiler compiler, String classToInit, GPRegister register) {
            compiler.getStackManager().pushParamForFunctionCall(register);
            Label labelInit = new Label("code.init." + classToInit);
            compiler.addInstruction(new BSR(labelInit));
            compiler.getStackManager().popParamForFunctionCall(register);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("new ");
        className.decompile(s);
        s.print("()");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        className.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        className.iter(f);
    }

}
