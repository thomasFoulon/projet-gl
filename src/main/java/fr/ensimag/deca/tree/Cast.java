package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.ErrorManager;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.FLOAT;
import fr.ensimag.ima.pseudocode.instructions.INT;
import java.io.PrintStream;

/**
 * Explicit cast.
 *
 * @author broissaw
 * @date 22/01/2020
 */
public class Cast extends AbstractExpr {

    private AbstractIdentifier newType;
    private AbstractExpr operand;

    public Cast(AbstractIdentifier ident, AbstractExpr operand) {
        this.newType = ident;
        this.operand = operand;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);

        Type tNew = newType.verifyType(compiler);
        Type tOld = this.operand.verifyExpr(compiler, localEnv, currentClass);

       if(tOld.castCompatible(compiler, tNew)){
           this.setType(tNew);
       }
       else{
           throw new ContextualError("Cast of (" + tOld.getName().getName() + ") in (" +  tNew.getName().getName() + ") is impossible.", this.newType.getLocation());
       }

       return this.getType();
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        if (this.newType.getType().sameType(this.operand.getType())) {
        // The object is already of the same type
            this.operand.codeGenExpr(compiler, numRegister);
        } else if (this.newType.getType().isFloat() && this.operand.getType().isInt()) {
        // Cast int to float
            this.operand.codeGenExpr(compiler, numRegister);
            compiler.addInstruction(new FLOAT(compiler.getR(numRegister), compiler.getR(numRegister)));
            compiler.addErrorInvalidTypeConversion();
        } else if (this.newType.getType().isInt() && this.operand.getType().isFloat()) {
        // Cast float to int
            this.operand.codeGenExpr(compiler, numRegister);
            compiler.addInstruction(new INT(compiler.getR(numRegister), compiler.getR(numRegister)));
            compiler.addErrorInvalidTypeConversion();
        } else if (this.newType.getType().isClass() && this.operand.getType().isClass()) {
        // Cast object to Class
            this.operand.codeGenExpr(compiler, numRegister);
            Label castOkLabel = compiler.getLabelManager().newCastOKLabel();
            // Test if operand is null
            compiler.addInstruction(new CMP(new NullOperand(), compiler.getR(numRegister)));
            compiler.addInstruction(new BEQ(castOkLabel));
            int numRegInstanceOf = RegisterManager.getNextRegister(compiler, numRegister);
            InstanceOf instanceOf = new InstanceOf(operand, newType);
            instanceOf.codeGenExpr(compiler, numRegInstanceOf);
            numRegInstanceOf = RegisterManager.getStoredRegister(compiler, numRegister, numRegInstanceOf);
            compiler.addInstruction(new CMP(new ImmediateInteger(1), compiler.getR(numRegInstanceOf)));
            compiler.addInstruction(new BEQ(castOkLabel));
            compiler.addErrorInvalidTypeConversion();
            compiler.addInstruction(new BRA(ErrorManager.INVALID_TYPE_CONVERSION));
            compiler.addLabel(castOkLabel);
        }
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        newType.decompile(s);
        s.print(") (");
        operand.decompile(s);
        s.print(")");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        newType.prettyPrint(s, prefix, false);
        operand.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        newType.iter(f);
        operand.iter(f);
    }
    
     /**
     * Generate the assembly code for the cast as a condition.
     * @param compiler the compiler
     * @param branchIfTrue branch on the label if the condition if true or not 
     * @param label the label where to branch
     */
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label){
        assert(this.getType().isBoolean());
        assert(compiler != null);
        assert(label != null);
        this.codeGenExpr(compiler, 2);
        compiler.addInstruction(new CMP(1, compiler.getR(2)));
        if(branchIfTrue){
            compiler.addInstruction(new BEQ(label));
        }
        else{
            compiler.addInstruction(new BNE(label));
        }
    }

}
