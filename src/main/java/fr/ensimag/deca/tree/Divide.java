package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.instructions.DIV;
import fr.ensimag.ima.pseudocode.instructions.QUO;

/**
 * Divide operation (/).
 * @author gl38
 * @date 01/01/2020
 */
public class Divide extends AbstractOpArith {
    public Divide(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }


    @Override
    protected String getOperatorName() {
        return "/";
    }

    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {
        assert(this.getType() != null);
        if (this.getType().isFloat()) {
            compiler.addInstruction(new DIV(dvalRight, compiler.getR(numRegister)));
        } else {            
            compiler.addInstruction(new QUO(dvalRight, compiler.getR(numRegister)));
            compiler.addErrorDivZeroInt();
        }
    }

}
