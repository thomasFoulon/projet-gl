package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Print statement (print, println, ...).
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractPrint extends AbstractInst {

    private final boolean printHex;
    private ListExpr arguments = new ListExpr();

    abstract String getSuffix();

    public AbstractPrint(boolean printHex, ListExpr arguments) {
        Validate.notNull(arguments);
        this.arguments = arguments;
        this.printHex = printHex;
    }

    public ListExpr getArguments() {
        return arguments;
    }

    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        // rules (3.19), (3.21), (3.31)
        // Verify the arguments of the print function
        assert(compiler != null);
        assert(localEnv != null);
        assert(returnType != null);
        for(AbstractExpr a : getArguments().getList()){
            // General verifications
            Type t = a.verifyExpr(compiler, localEnv, currentClass);
            // Verification this is a print parameter
            if(!t.isInt() && !t.isFloat() && !t.isString()){ // condition in the rule (3.31)
                throw new ContextualError("print is not applicable for the arguments of type (" + t.toString() + ")", a.getLocation());
            }
        }
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        for (AbstractExpr a : getArguments().getList()) {
            a.codeGenPrint(compiler, this.getPrintHex());
        }
    }

    private boolean getPrintHex() {
        return printHex;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("print" + this.getSuffix());
        s.print((this.getPrintHex()?"x":"") + "(");
        this.arguments.decompile(s);
        s.print(");");
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        arguments.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        arguments.prettyPrint(s, prefix, true);
    }

}
