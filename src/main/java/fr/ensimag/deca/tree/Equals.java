package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.SEQ;


/**
 * Equals comparison (==).
 * @author gl38
 * @date 01/01/2020
 */
public class Equals extends AbstractOpExactCmp {

    public Equals(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "==";
    }

    @Override
    protected Instruction getInstruction(GPRegister destReg) {
        return new SEQ(destReg);
    }

    @Override
    protected void branch(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        if(branchIfTrue){
            compiler.addInstruction(new BEQ(label));
        }
        else{
            compiler.addInstruction(new BNE(label));
        }
    }
    
}
