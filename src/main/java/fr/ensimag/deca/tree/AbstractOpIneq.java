package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;


/**
 * Inequality expressions (<, >, <=, >=).
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractOpIneq extends AbstractOpCmp {

    public AbstractOpIneq(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    protected void verifyType(DecacCompiler compiler, Type tLeft, Type tRight) throws ContextualError {        
        if (!(tLeft.isInt() || tLeft.isFloat())) {
            throw new ContextualError("Left operand must be of type (int) or (float).", this.getLeftOperand().getLocation());
        }
        if (!(tRight.isInt() || tRight.isFloat())) {
            throw new ContextualError("Right operand must be of type (int) or (float).", this.getRightOperand().getLocation());
        }
    }
    
}
