package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;

/**
 * Class declaration.
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractDeclClass extends Tree {

    /**
     * Pass 1 of [SyntaxeContextuelle]. Verify that the class declaration is OK
     * without looking at its content.
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError if there is a contextual error
     */
    protected abstract void verifyClass(DecacCompiler compiler)
            throws ContextualError;

    /**
     * Pass 2 of [SyntaxeContextuelle]. Verify that the class members (fields and
     * methods) are OK, without looking at method body and field initialization.
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError if there is a contextual error
     */
    protected abstract void verifyClassMembers(DecacCompiler compiler)
            throws ContextualError;

    /**
     * Pass 3 of [SyntaxeContextuelle]. Verify that instructions and expressions
     * contained in the class are OK.
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError if there is a contextual error
     */
    protected abstract void verifyClassBody(DecacCompiler compiler)
            throws ContextualError;

    /**
     * Generate the code to initialize the class and create the method init
     * if it is necessary (if there are fields in the class)
     * @param compiler 
     */
    protected abstract void codeGenInitialization(DecacCompiler compiler) throws ContextualError;
    
    /**
     * Generate the code to create the method table for the class in the stack
     * @param compiler 
     */
    protected abstract void codeGenMethodTable(DecacCompiler compiler) throws ContextualError;
    
    /**
     * Generate the code to add all method body at the end of the main program
     * @param compiler 
     */
    protected abstract void codeGenMethodBody(DecacCompiler compiler);

}
