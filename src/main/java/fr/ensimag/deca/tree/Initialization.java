package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Initialization.
 * @author gl38
 * @date 01/01/2020
 */
public class Initialization extends AbstractInitialization {

    public AbstractExpr getExpression() {
        return expression;
    }

    private AbstractExpr expression;

    public void setExpression(AbstractExpr expression) {
        Validate.notNull(expression);
        this.expression = expression;
    }

    public Initialization(AbstractExpr expression) {
        Validate.notNull(expression);
        this.expression = expression;
    }

    @Override
    protected void verifyInitialization(DecacCompiler compiler, Type t,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        assert(compiler != null);
        assert(t != null);
        assert(localEnv != null);
        AbstractExpr e = expression.verifyRValue(compiler, localEnv, currentClass, t);
        // In case of a ConvFloat
        expression = e;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(" = ");
        this.expression.decompile(s);
    }

    @Override
    protected
    void iterChildren(TreeFunction f) {
        getExpression().iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        expression.prettyPrint(s, prefix, true);
    }

    @Override
    protected void codeGenInitialization(DecacCompiler compiler, DAddr operand, boolean isClass) {
        int numReg = 2;
        expression.codeGenExpr(compiler, numReg);
        compiler.addInstruction(new STORE(compiler.getR(numReg), operand));
    }

    @Override
    protected void codeGenInitField(DecacCompiler compiler, DAddr operand, boolean isClass) {
        expression.codeGenExpr(compiler, 3);
        compiler.addInstruction(new STORE(compiler.getR(3), operand));
    }

}
