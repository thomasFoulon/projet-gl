package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.instructions.OPP;

/**
 * Unary minus (ex: -9);
 * @author gl38
 * @date 01/01/2020
 */
public class UnaryMinus extends AbstractUnaryExpr {

    public UnaryMinus(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {

        Type t = this.getOperand().verifyExpr(compiler, localEnv, currentClass);

        if(!(t.isInt() || t.isFloat())){
            throw new ContextualError("Operand must be of type (int) or (float).", this.getOperand().getLocation());
        }
        else{
            this.setType(t);
            return t;
        }
    }

    @Override
    protected String getOperatorName() {
        return "-";
    }

    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {
        compiler.addInstruction(new OPP(dvalRight, compiler.getR(numRegister)));
    }

}
