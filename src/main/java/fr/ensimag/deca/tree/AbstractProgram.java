package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;

/**
 * Entry point for contextual verifications and code generation from outside the package.
 * 
 * @author gl38
 * @date 01/01/2020
 *
 */
public abstract class AbstractProgram extends Tree {
    
    /**
     * Implements non-terminal "program" of [SyntaxeContextuelle] in pass 1, 2 and 3
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError if there is a contextual error
     */
    public abstract void verifyProgram(DecacCompiler compiler) throws ContextualError;
    
    /**
     * Generate the code for the whole program with its class and main
     * @param compiler the compiler
     */
    public abstract void codeGenProgram(DecacCompiler compiler) throws ContextualError;

}
