package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.context.TypeDefinition;
import fr.ensimag.deca.context.VariableDefinition;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Deca Identifier
 *
 * @author gl38
 * @date 01/01/2020
 */
public class Identifier extends AbstractIdentifier {

    @Override
    protected void checkDecoration() {
        if (getDefinition() == null) {
            throw new DecacInternalError("Identifier " + this.getName() + " has no attached Definition");
        }
    }

    @Override
    public Definition getDefinition() {
        return definition;
    }

    @Override
    public ClassDefinition getClassDefinition() {
        try {
            return (ClassDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a class identifier, you can't call getClassDefinition on it");
        }
    }

    @Override
    public MethodDefinition getMethodDefinition() {
        try {
            return (MethodDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a method identifier, you can't call getMethodDefinition on it");
        }
    }

    @Override
    public FieldDefinition getFieldDefinition() {
        try {
            return (FieldDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a field identifier, you can't call getFieldDefinition on it");
        }
    }

    @Override
    public VariableDefinition getVariableDefinition() {
        try {
            return (VariableDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a variable identifier, you can't call getVariableDefinition on it");
        }
    }

    @Override
    public ExpDefinition getExpDefinition() {
        try {
            return (ExpDefinition) definition;
        } catch (ClassCastException e) {
            throw new DecacInternalError(
                    "Identifier "
                            + getName()
                            + " is not a Exp identifier, you can't call getExpDefinition on it");
        }
    }

    @Override
    public void setDefinition(Definition definition) {
        this.definition = definition;
    }

    @Override
    public Symbol getName() {
        return name;
    }

    private final Symbol name;

    public Identifier(Symbol name) {
        Validate.notNull(name);
        this.name = name;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        ExpDefinition expDefinition = localEnv.get(this.getName());
        if (expDefinition == null) {
            throw new ContextualError("The variable " + this.getName() + " is not declared.", this.getLocation());
        }
        this.setDefinition(expDefinition);
        this.setType(expDefinition.getType());
        return expDefinition.getType();
    }

    @Override
    public Type verifyType(DecacCompiler compiler) throws ContextualError {
        assert(compiler != null);
        TypeDefinition typeDefinition = compiler.getEnvironmentType().get(this.getName());
        if(typeDefinition == null){
            throw new ContextualError("The type " + this.getName() + " doesn't exist or is not declarable.", this.getLocation());
        }
        Type t = typeDefinition.getType();
        this.setDefinition(typeDefinition);
        this.setType(t);
        return t;
    }

    private Definition definition;

    @Override
    protected void iterChildren(TreeFunction f) {
        // leaf node => nothing to do
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // leaf node => nothing to do
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print(name.toString());
    }

    @Override
    String prettyPrintNode() {
        return "Identifier (" + getName() + ")";
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Definition d = getDefinition();
        if (d != null) {
            s.print(prefix);
            s.print("definition: ");
            s.print(d);
            s.println();
        }
    }
    
    private DAddr loadOperand(DecacCompiler compiler, int numRegister) {        
        if (this.definition.isField()) {
            FieldDefinition fieldDef = (FieldDefinition) this.definition;
            // we are in a method and we are trying to access a field of the object (with no this specified)
            if (fieldDef.getOperand() == null) {
                compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), compiler.getR(numRegister)));
            }
            return new RegisterOffset(fieldDef.getIndex(), compiler.getR(numRegister));
        }
        if (this.definition.isParam()){
            ParamDefinition paramDef = (ParamDefinition) this.definition;
            return new RegisterOffset(paramDef.getOffset(), Register.LB);
        }
        return null;
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        DVal dvalExpr = this.getDVal();
        if (this.definition.isField() || this.definition.isParam()) {
            dvalExpr = this.loadOperand(compiler, numRegister);
        }
        compiler.addInstruction(
                new LOAD(dvalExpr,
                compiler.getR(numRegister))
        );
    }

    @Override
    protected DVal getDVal() {
        if(definition.isExpression()){
            return ((ExpDefinition) definition).getOperand();
        }
        return null;
    }

    @Override
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        assert(getType().isBoolean());
        assert(compiler != null);
        assert(label != null);
        this.codeGenExpr(compiler, 2);
        compiler.addInstruction(new CMP(1, compiler.getR(2)));
        if(branchIfTrue){
            compiler.addInstruction(new BEQ(label));
        }
        else{
            compiler.addInstruction(new BNE(label));
        }
    }

    @Override
    public FieldDefinition verifyFieldIdent(EnvironmentExp members) throws ContextualError {
        assert(members != null);
        ExpDefinition def = members.get(this.name);
        if(def == null){
            throw new ContextualError("The field " + this.name + " is not defined.", this.getLocation());
        }
        FieldDefinition fieldDef = def.asFieldDefinition(this.name + " is not a field.", this.getLocation());
        this.setDefinition(fieldDef);
        this.setType(fieldDef.getType());
        return fieldDef;
    }

    @Override
    public MethodDefinition verifyMethodIdent(EnvironmentExp members) throws ContextualError {
        assert(members != null);
        ExpDefinition def = members.get(this.name);
        if(def == null){
            throw new ContextualError("The method " + this.name + " is not defined.", this.getLocation());
        }
        MethodDefinition methodDef = def.asMethodDefinition(this.name + " is not a method.", this.getLocation());
        this.setDefinition(methodDef);
        this.setType(methodDef.getType());
        return methodDef;
    }

    @Override
    public DAddr getDAddr(DecacCompiler compiler, int numReg) {
        if (this.getExpDefinition().getOperand() == null) {
            return this.loadOperand(compiler, numReg);
        }
        return this.getExpDefinition().getOperand();
    }
}
