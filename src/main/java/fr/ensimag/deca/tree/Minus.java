package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.instructions.SUB;

/**
 * Minus operation (-).
 * @author gl38
 * @date 01/01/2020
 */
public class Minus extends AbstractOpArith {

    public Minus(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "-";
    }

    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {
        compiler.addInstruction(new SUB(dvalRight, compiler.getR(numRegister)));
    }

}
