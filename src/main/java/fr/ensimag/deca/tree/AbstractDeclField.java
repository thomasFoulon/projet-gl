/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.GPRegister;

/**
 * Field declaration.
 * @author gl38
 * @date 17/01/2020
 */
public abstract class AbstractDeclField extends Tree {   

    /**
     * Generate the assembly code to initialize the field.
     * @param compiler the compiler
     * @param regAddr the register containing the address of the object
     */
    public abstract void codeGenDeclField(DecacCompiler compiler, GPRegister regAddr);

    /**
     * Implements non-terminal "decl_field" of [SyntaxeContextuelle] in pass 2
     * warning: the potential DoubleDef contextual error in rules (2.3) & (2.4) is handled here.
     * @param compiler contains the "env_types" attribute
     * @param superClass the symbol of the superclass
     * @param className the symbol of the class
     * @param classDef the class definition containing the members where to add the field definitions
     * @throws ContextualError if there is a contextual error
     */
    public abstract void verifyDeclField(DecacCompiler compiler, SymbolTable.Symbol superClass, SymbolTable.Symbol className, ClassDefinition classDef) throws ContextualError;

    /**
     * Implements non-terminal "decl_field" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains the "env_types" attribute
     * @param classDef the class definition containing the members of the class
     * @throws ContextualError if there is a contextual error
     */
    public abstract void verifyDeclFieldInitialization(DecacCompiler compiler, ClassDefinition classDef) throws ContextualError;

}
