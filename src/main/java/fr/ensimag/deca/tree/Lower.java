package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BGE;
import fr.ensimag.ima.pseudocode.instructions.BLT;
import fr.ensimag.ima.pseudocode.instructions.SLT;

/**
 * Lower comparison (<).
 * @author gl38
 * @date 01/01/2020
 */
public class Lower extends AbstractOpIneq {

    public Lower(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return "<";
    }

    @Override
    protected Instruction getInstruction(GPRegister destReg) {
        return new SLT(destReg);
    }

    @Override
    protected void branch(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        if(branchIfTrue){
            compiler.addInstruction(new BLT(label));
        }
        else{
            compiler.addInstruction(new BGE(label));
        }
    }

}
