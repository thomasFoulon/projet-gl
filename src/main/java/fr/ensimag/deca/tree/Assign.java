package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.instructions.STORE;

/**
 * Assignment, i.e. lvalue = expr.
 *
 * @author gl38
 * @date 01/01/2020
 */
public class Assign extends AbstractBinaryExpr {

    @Override
    public AbstractLValue getLeftOperand() {
        // The cast succeeds by construction, as the leftOperand has been set
        // as an AbstractLValue by the constructor.
        return (AbstractLValue)super.getLeftOperand();
    }

    public Assign(AbstractLValue leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        Type t = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        AbstractExpr rightOperand = this.getRightOperand().verifyRValue(compiler, localEnv, currentClass, t);
        // In case of conversion
        this.setRightOperand(rightOperand);
        this.setType(t);
        return t;
    }


    @Override
    protected String getOperatorName() {
        return "=";
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        this.getRightOperand().codeGenExpr(compiler, numRegister);
        int numRegLeft = RegisterManager.getNextRegister(compiler, numRegister);
        DAddr daddrVar = this.getLeftOperand().getDAddr(compiler, numRegLeft);
        RegisterManager.getStoredRegister(compiler, numRegister, numRegLeft);
        this.codeGenOperation(compiler, daddrVar, numRegister);
    }
    

    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {
        compiler.addInstruction(new STORE(compiler.getR(numRegister), (DAddr) dvalRight));
    }

}
