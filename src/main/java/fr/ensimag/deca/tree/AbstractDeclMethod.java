/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.SymbolTable;

/**
 * Method declaration.
 * @author gl38
 * @date 17/01/2020
 */
public abstract class AbstractDeclMethod extends Tree {

    /**
     * Implements non-terminal "decl_method" of [SyntaxeContextuelle] in pass 2
     * warning: the potential DoubleDef contextual error in rules (2.3) & (2.6) is handled here.
     * @param compiler contains the "env_types" attribute
     * @param superClass the symbol of the superclass
     * @param classDef the class definition containing the members where to add the field definitions
     * @throws ContextualError if there is a contextual error
     */
    public abstract void verifyDeclMethod(DecacCompiler compiler, SymbolTable.Symbol superClass, ClassDefinition classDef) throws ContextualError;

    /**
     * Implements non-terminal "decl_method" of [SyntaxeContextuelle] in pass 3
     * warning: the potential DoubleDef contextual error in rule (2.6) is handled here.
     * @param compiler contains the "env_types" attribute
     * @param classDef the class definition containing the members of the class
     * @throws ContextualError if there is a contextual error
     */
    public abstract void verifyDeclMethodBody(DecacCompiler compiler, ClassDefinition classDef) throws ContextualError;

    /**
     * Add this method to the method table of the class given
     * It only set the operand of the method and its parameter according to the
     * offset of the class in the stack
     * @param compiler
     * @param classDef the class of the method
     * @param className the name of the class
     * @param offsetClass the offset of the class in the stack (to set the operand)
     */
    protected abstract void codeGenMethodTable(DecacCompiler compiler, ClassDefinition classDef, String className, int offsetClass) throws ContextualError;

    /**
     * Generate the code for the method body
     * @param compiler 
     */
    protected abstract void codeGenMethodBody(DecacCompiler compiler);
}
