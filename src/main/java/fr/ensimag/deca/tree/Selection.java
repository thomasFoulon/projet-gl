/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BNE;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import java.io.PrintStream;

/**
 * Selection expression (ex: this.getA().printX()).
 * @author gl38
 * @date 17/01/2020
 */
public class Selection extends AbstractLValue {

    private final AbstractExpr object;
    private final AbstractIdentifier selectedField;

    public Selection(AbstractExpr object, AbstractIdentifier selectedField) {
        this.object = object;
        this.selectedField = selectedField;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        Type objectType = object.verifyExpr(compiler, localEnv, currentClass);
        if(!objectType.isClass()){
            throw new ContextualError(objectType.getName() + " cannot be dereferenced.", this.getLocation());
        }
        Definition def = compiler.getEnvironmentType().get(objectType.getName());
        ClassDefinition exprClassDef = (ClassDefinition) def;
        FieldDefinition fieldDef = selectedField.verifyFieldIdent(exprClassDef.getMembers());
        // If the field is protected, we have to verify additionnal conditions
        if(fieldDef.getVisibility() == Visibility.PROTECTED){
            if(currentClass == null){
                throw new ContextualError("The protected field " + selectedField.getName() + " cannot be used in the main program.", this.getLocation());
            }
            if(!currentClass.getType().subtype(fieldDef.getContainingClass().getType())){
                throw new ContextualError("The type of the current class must be a subclass of " + fieldDef.getContainingClass().getType().getName() + ".", this.getLocation());
            }
            if(!exprClassDef.getType().subtype(currentClass.getType())){
                throw new ContextualError("The type of the expression must be a subtype of " + currentClass.getType().getName() + ".", this.getLocation());
            }
        }
        this.setType(fieldDef.getType());
        return fieldDef.getType();
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
       this.object.codeGenExpr(compiler, numRegister);
       compiler.addErrorNullDereference(numRegister);
       FieldDefinition fieldDef = this.selectedField.getFieldDefinition();
       fieldDef.setOperand(new RegisterOffset(fieldDef.getIndex(), compiler.getR(numRegister)));
       this.selectedField.codeGenExpr(compiler, numRegister);
       fieldDef.setOperand(null);
    }

    @Override
    public void decompile(IndentPrintStream s) {
        object.decompile(s);
        s.print(".");
        selectedField.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        object.prettyPrint(s, prefix, false);
        selectedField.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        object.iter(f);
        selectedField.iter(f);
    }

    @Override
    public DAddr getDAddr(DecacCompiler compiler, int numReg) {
       this.object.codeGenExpr(compiler, numReg);
       compiler.addErrorNullDereference(numReg);
       FieldDefinition fieldDef = this.selectedField.getFieldDefinition();
       return new RegisterOffset(fieldDef.getIndex(), compiler.getR(numReg));
    }

    @Override
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        assert(this.getType().isBoolean());
        assert(compiler != null);
        assert(label != null);
        this.codeGenExpr(compiler, 2);
        compiler.addInstruction(new CMP(1, compiler.getR(2)));
        if(branchIfTrue){
            compiler.addInstruction(new BEQ(label));
        }
        else{
            compiler.addInstruction(new BNE(label));
        }
    }
}
