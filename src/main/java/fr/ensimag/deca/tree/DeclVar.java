package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.VariableDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Variable declaration.
 * @author gl38
 * @date 01/01/2020
 */
public class DeclVar extends AbstractDeclVar {


    final private AbstractIdentifier type;
    final private AbstractIdentifier varName;
    final private AbstractInitialization initialization;

    public DeclVar(AbstractIdentifier type, AbstractIdentifier varName, AbstractInitialization initialization) {
        Validate.notNull(type);
        Validate.notNull(varName);
        Validate.notNull(initialization);
        this.type = type;
        this.varName = varName;
        this.initialization = initialization;
    }

    @Override
    protected void verifyDeclVar(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        Type t = type.verifyType(compiler);
        if(t.isVoid()){
            throw new ContextualError("Cannot declare a (void) type variable.", this.getLocation());
        }
        SymbolTable.Symbol s = varName.getName();
        initialization.verifyInitialization(compiler, t, localEnv, currentClass);
        try {
            VariableDefinition varDef = new VariableDefinition(t, this.getLocation());
            localEnv.declare(s, varDef);
            varName.setDefinition(varDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            throw new ContextualError("Variable " + s.getName() + " is already declared.", this.getLocation());
        }
    }


    @Override
    public void decompile(IndentPrintStream s) {
        this.type.decompile(s);
        s.print(" ");
        this.varName.decompile(s);
        this.initialization.decompile(s);
        s.println(";");
    }

    @Override
    protected
    void iterChildren(TreeFunction f) {
        type.iter(f);
        varName.iter(f);
        initialization.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        varName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }

    @Override
    protected void codeGenDeclVar(DecacCompiler compiler) {
        assert(compiler != null);
        // Declare the variable in the stack
        compiler.getStackManager().declareVariable(
                varName.getVariableDefinition()
        );
        // Generate the code
        initialization.codeGenInitialization(
                compiler,
                varName.getVariableDefinition().getOperand(), varName.getVariableDefinition().getType().isClass()
        );
    }
}
