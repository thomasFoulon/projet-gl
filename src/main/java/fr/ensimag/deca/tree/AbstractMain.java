package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;

/**
 * Main block of a Deca program.
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractMain extends Tree {

    /**
     * Generate the code for the main declarations and instructions
     * @param compiler 
     */
    protected abstract void codeGenMain(DecacCompiler compiler);


    /**
     * Implements non-terminal "main" of [SyntaxeContextuelle] in pass 3 
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError
     */
    protected abstract void verifyMain(DecacCompiler compiler) throws ContextualError;
}
