package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.ImmediateFloat;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BEQ;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import java.io.PrintStream;

/**
 * instanceof expression.
 * @author gl38
 * @date 01/01/2020
 */
public class InstanceOf extends AbstractExpr {

    private AbstractExpr object;
    private AbstractIdentifier testedType;
    
    public InstanceOf(AbstractExpr object, AbstractIdentifier testedType) {
        this.object = object;
        this.testedType = testedType;
    }
    
    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        this.object.verifyExpr(compiler, localEnv, currentClass);
        Type t = this.testedType.verifyType(compiler);
        if (!object.getType().isClassOrNull()) {
            throw new ContextualError("The expression must be an object or null.", object.getLocation());
        }
        if (!t.isClass()) {
            throw new ContextualError("The tested type must be a class.", testedType.getLocation());
        }
        this.setType(compiler.getType("boolean"));
        return this.getType();
    }
    
    /**
     * Generate the code to create the verification of the dynamic class of the object
     * @param compiler the compiler
     * @param register the register where the address of the object is stored
     * @param instanceOfLabels the labels used in the instanceOf
     *                          (start, end, true and false as returned by the LabelManager)
     */
    private void codeGenInstanceOf(DecacCompiler compiler, GPRegister register, Label[] instanceOfLabels) {
        // if the object is null return false
        compiler.addInstruction(new CMP(new NullOperand(), register));
        compiler.addInstruction(new BEQ(instanceOfLabels[3])); // go to instanceOfFalse
        // create a while to test if the object is of type testedType
        compiler.addInstruction(new LEA(this.testedType.getClassDefinition().getOperand(), Register.R0));
        compiler.addLabel(instanceOfLabels[0]); // add InstanceOfStart label
        compiler.addInstruction(new LOAD(new RegisterOffset(0, register), register));
        compiler.addInstruction(new CMP(Register.R0, register));
        compiler.addInstruction(new BEQ(instanceOfLabels[2])); // go to instanceOfTrue
        // Compare current address to decompile to the base (null)
        compiler.addInstruction(new CMP(new RegisterOffset(1, Register.GB), register));
        compiler.addInstruction(new BEQ(instanceOfLabels[3])); // go to instanceOfFalse
        compiler.addInstruction(new BRA(instanceOfLabels[0])); // go back to instanceOfStart
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        this.object.codeGenExpr(compiler, numRegister);
        GPRegister register = compiler.getR(numRegister);        
        Label[] instanceOfLabels = compiler.getLabelManager().newInstanceOfLabels();
        this.codeGenInstanceOf(compiler, register, instanceOfLabels);
        compiler.addLabel(instanceOfLabels[2]); // add InstanceOfTrue label
        compiler.addInstruction(new LOAD(new ImmediateInteger(1), register));
        compiler.addInstruction(new BRA(instanceOfLabels[1])); // go to instanceOfEnd        
        compiler.addLabel(instanceOfLabels[3]); // add InstanceOfFalse label
        compiler.addInstruction(new LOAD(new ImmediateInteger(0), register));
        compiler.addLabel(instanceOfLabels[1]); // add InstanceOfEnd label
    }

    @Override
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        this.object.codeGenExpr(compiler, 2);
        GPRegister register = compiler.getR(2);
        Label[] instanceOfLabels = compiler.getLabelManager().newInstanceOfLabels();
        this.codeGenInstanceOf(compiler, register, instanceOfLabels);
        
        compiler.addLabel(instanceOfLabels[2]); // add InstanceOfTrue label
        if (branchIfTrue) {
            compiler.addInstruction(new BRA(label));
        } else {
            compiler.addInstruction(new BRA(instanceOfLabels[1]));
        }
        
        compiler.addLabel(instanceOfLabels[3]); // add InstanceOfFalse label
        if (!branchIfTrue) {
            compiler.addInstruction(new BRA(label));
        }
        compiler.addLabel(instanceOfLabels[1]); // add InstanceOfEnd label
    }
    
    @Override
    public void decompile(IndentPrintStream s) {
        object.decompile(s);
        s.print(" instanceof ");
        testedType.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        object.prettyPrint(s, prefix, false);
        testedType.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        object.iter(f);
        testedType.iter(f);
    }

    

}
