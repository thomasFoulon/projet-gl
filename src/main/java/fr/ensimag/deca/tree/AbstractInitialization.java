package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DAddr;

/**
 * Initialization (of variable, field, ...).
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractInitialization extends Tree {
    
    /**
     * Implements non-terminal "initialization" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains "env_types" attribute
     * @param t corresponds to the "type" attribute
     * @param localEnv corresponds to the "env_exp" attribute
     * @param currentClass 
     *          corresponds to the "class" attribute (null in the main bloc).
     * @throws ContextualError if there is a contextual error
     */
    protected abstract void verifyInitialization(DecacCompiler compiler,
            Type t, EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError;

    /**
     * Generate the code to initialize the variable stored at operand if necessary
     * @param compiler
     * @param operand address of the variable that need to be initialized
     */
    protected abstract void codeGenInitialization(DecacCompiler compiler, DAddr operand, boolean isClass);
    
    /**
     * Generate the code to initialize the field stored at operand
     * It initialize with zero if no initialization was specified
     * @param compiler
     * @param operand address of the field that need to be initialized
     */
    protected abstract void codeGenInitField(DecacCompiler compiler, DAddr operand, boolean isClass);
    
}
