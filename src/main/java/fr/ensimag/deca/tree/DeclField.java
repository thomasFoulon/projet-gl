/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.context.TypeDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import java.io.PrintStream;

/**
 * Field declaration in a class.
 * @author gl38
 * @date 17/01/2020
 */
public class DeclField extends AbstractDeclField {
    
    final private Visibility visibility;
    final private AbstractIdentifier type;
    final private AbstractIdentifier fieldName;
    final private AbstractInitialization initialization;
    
    public DeclField(Visibility visibility, AbstractIdentifier type,
                     AbstractIdentifier fieldName, AbstractInitialization initialization) {
        this.visibility = visibility;
        this.type = type;
        this.fieldName = fieldName;
        this.initialization = initialization;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        if (visibility == Visibility.PROTECTED) {
            s.print("protected");
            s.print(" ");
        }
        this.type.decompile(s);
        s.print(" ");
        this.fieldName.decompile(s);
        this.initialization.decompile(s);
        s.println(";");
    }

    @Override
    String prettyPrintNode() {
        String v = (visibility == Visibility.PROTECTED ? "PROTECTED" : "PUBLIC");
        String res = "[visibility=" + v + "] ";
        res += this.getClass().getSimpleName();
        return res;
    }
    
    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        fieldName.prettyPrint(s, prefix, false);
        initialization.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        fieldName.iter(f);
        initialization.iter(f);
    }

    @Override
    public void verifyDeclField(DecacCompiler compiler, SymbolTable.Symbol superClass, SymbolTable.Symbol className, ClassDefinition classDef) throws ContextualError{
        assert(compiler != null);
        assert(superClass != null);
        assert(className != null);
        assert(classDef != null);
        assert(classDef.getMembers() != null);
        // --- Rule (2.5) conditions ---
        if(compiler.getType(this.type.getName()).isVoid()){
            throw new ContextualError("The field " + this.fieldName.getName() + " cannot be of type void.", this.getLocation());
        }
        // Verifying the field isn't a method in the superclass
        EnvironmentExp envExpSuper = classDef.getMembers().getParent();
        ExpDefinition superFieldDef = envExpSuper.get(fieldName.getName());
        if(superFieldDef != null){
            // The name is defined in the superclass
            // Is it a field ? -> if yes : no problem, else (it's a method) : error.
            superFieldDef.asFieldDefinition("The field " + fieldName.getName() + " cannot be a method in the superclass " + superClass.getName() + ".", this.getLocation());
        }
        // Creating the new FieldDefinition
        classDef.incNumberOfFields();
        Type typeT = compiler.getType(this.type.getName().getName());
        ExpDefinition fieldDef = new FieldDefinition(typeT, this.getLocation(), visibility, classDef, classDef.getNumberOfFields());
        try{
            // Declaration of the field in the members of the class
            classDef.getMembers().declare(fieldName.getName(), fieldDef);
        } catch (EnvironmentExp.DoubleDefException ex){
            // --- Rule (2.4) condition ---
            throw new ContextualError("The field/method " + fieldName.getName() + " is already defined.", this.getLocation());
        }
        // Decorations
        TypeDefinition typeDef = compiler.getEnvironmentType().get(this.type.getName());
        this.type.setDefinition(typeDef);
        this.type.setType(typeDef.getType());
        this.fieldName.setDefinition(fieldDef);
        this.fieldName.setType(typeDef.getType());
    }

    @Override
    public void codeGenDeclField(DecacCompiler compiler, GPRegister regAddr) {
        int index = fieldName.getFieldDefinition().getIndex();
        RegisterOffset regForField = new RegisterOffset(index, regAddr);
        this.initialization.codeGenInitField(compiler, regForField, fieldName.getFieldDefinition().getType().isClass());
    }

    @Override
    public void verifyDeclFieldInitialization(DecacCompiler compiler, ClassDefinition classDef) throws ContextualError {
        this.initialization.verifyInitialization(compiler, this.type.getType(), classDef.getMembers(), classDef);
    }
    
}
