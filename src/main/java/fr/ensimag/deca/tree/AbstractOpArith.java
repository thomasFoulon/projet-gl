package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;

/**
 * Arithmetic binary operations (+, -, /, ...).
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractOpArith extends AbstractBinaryExpr {

    public AbstractOpArith(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        // Verifying the left and the right operand and getting their type
        Type tLeft = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type tRight = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!(tLeft.isInt() || tLeft.isFloat())) {
            throw new ContextualError("Left operand must be of type (int) or (float).", this.getLeftOperand().getLocation());
        }
        if (!(tRight.isInt() || tRight.isFloat())) {
            throw new ContextualError("Right operand must be of type (int) or (float).", this.getRightOperand().getLocation());
        }
        if (tLeft.isFloat() || tRight.isFloat()) {
            // If one of the two operands is of thype float and the other is of type int : ConvFloat
            this.setType(compiler.getType("float"));
            if (tLeft.isInt()) {
                this.setLeftOperand(new ConvFloat(this.getLeftOperand()));
                this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
            } else if (tRight.isInt()) {
                this.setRightOperand(new ConvFloat(this.getRightOperand()));
                this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
            }
            return this.getType();
        }
        else {
            this.setType(compiler.getType("int"));
            return this.getType();
        }
    }
    
    
    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        super.codeGenExpr(compiler, numRegister);
        if (this.getType().isFloat()) {
            compiler.addErrorOverflowFloat();
        }
    
    }
}
