package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.ImmediateInteger;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.SEQ;

/**
 * Not expression (!).
 * @author gl38
 * @date 01/01/2020
 */
public class Not extends AbstractUnaryExpr {

    public Not(AbstractExpr operand) {
        super(operand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        Type t = this.getOperand().verifyExpr(compiler, localEnv, currentClass);
        if (!(t.isBoolean())) {
            throw new ContextualError("Operand must be of type (boolean).", this.getOperand().getLocation());
        }
        else {
            this.setType(compiler.getType("boolean"));
            return this.getType();
        }
    }

    @Override
    protected String getOperatorName() {
        return "!";
    }

    @Override
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        getOperand().codeGenCondition(compiler, !branchIfTrue, label);
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        DVal valOper = this.getOperand().getDVal();
        if (valOper == null) {
            this.getOperand().codeGenExpr(compiler, numRegister);
            valOper = compiler.getR(numRegister);
        }
        else {
            compiler.addInstruction(new LOAD(valOper, compiler.getR(numRegister)));
        }
        this.codeGenOperation(compiler, valOper, numRegister);
    }

    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {
        compiler.addInstruction(new CMP(new ImmediateInteger(0), compiler.getR(numRegister)));
        compiler.addInstruction(new SEQ(compiler.getR(numRegister)));
    }

}
