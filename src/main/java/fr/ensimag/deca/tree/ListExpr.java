package fr.ensimag.deca.tree;

import fr.ensimag.deca.tools.IndentPrintStream;

/**
 * List of expressions (eg list of parameters).
 *
 * @author gl38
 * @date 01/01/2020
 */
public class ListExpr extends TreeList<AbstractExpr> {

    @Override
    public void decompile(IndentPrintStream s) {
        boolean printIsEmpty = true;
        for (AbstractExpr expr : this.getList()) {
            if (printIsEmpty) {
                printIsEmpty = false;
            } else {
                s.print(", ");
            }
            expr.decompile(s);
        }
    }
}
