/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;

/**
 * List of method declarations.
 * @author gl38
 * @date 01/01/2020
 */
public class ListDeclMethod extends TreeList<AbstractDeclMethod>{

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclMethod declMethod : this.getList()) {
            declMethod.decompile(s);
        }
    }

    /**
     * Implements non-terminal "list_decl_method" of [SyntaxeContextuelle] in pass 2
     * @param compiler contains the "env_types" attribute
     * @param superClass the symbol of the superclass
     * @param classDef the class definition containing the members where to add the method definitions
     * @throws ContextualError if there is a contextual error
     */
    public void verifyListDeclMethod(DecacCompiler compiler, SymbolTable.Symbol superClass, ClassDefinition classDef) throws ContextualError {
        assert(compiler != null);
        assert(superClass != null);
        EnvironmentExp env = new EnvironmentExp(null);
        for(AbstractDeclMethod m : this.getList()){
            // The potential (2.4) rule error is verified in verifyDeclField
            m.verifyDeclMethod(compiler, superClass, classDef);
        }
    }

    /**
     * Implements non-terminal "list_decl_method" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains the "env_types" attribute
     * @param classDef the class definition containing the members of the class
     * @throws ContextualError if there is a contextual error
     */
    public void verifyListDeclMethodBody(DecacCompiler compiler, ClassDefinition classDef) throws ContextualError {
        for(AbstractDeclMethod m : this.getList()){
            m.verifyDeclMethodBody(compiler, classDef);
        }
    }
    
    /**
     * Add all methods to the method table of the class given
     * It only set the operand of the method and its parameter according to the
     * offset of the class in the stack
     * @param compiler
     * @param classDef the class of the method
     * @param className the name of the class
     * @param offsetClass the offset of the class in the stack (to set the operand)
     */
    protected void codeGenMethodTable(DecacCompiler compiler, ClassDefinition classDef, String className, int offsetClass) 
        throws ContextualError {
        for (AbstractDeclMethod m : this.getList()) {
            m.codeGenMethodTable(compiler, classDef, className, offsetClass);
        }
    }

    /**
     * Generate the code for all the method body
     * @param compiler 
     */
    protected void codeGenMethodBody(DecacCompiler compiler) {
        for (AbstractDeclMethod m : this.getList()) {
            m.codeGenMethodBody(compiler);
        }
    }
    
}
