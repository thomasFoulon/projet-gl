/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ClassType;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.Signature;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.context.TypeDefinition;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import java.io.PrintStream;

/**
 * Declaration of a method.
 *
 * @author gl38
 * @date 20/01/2020
 */
public class DeclMethod extends AbstractDeclMethod {
    
    final private AbstractIdentifier returnType;
    final private AbstractIdentifier methodName;
    final private ListDeclParam params;
    final private AbstractMethodBody body;
    
    public DeclMethod(AbstractIdentifier returnType, AbstractIdentifier methodName,
                      ListDeclParam params, AbstractMethodBody body) {
        this.returnType = returnType;
        this.methodName = methodName;
        this.params = params;
        this.body = body;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        returnType.decompile(s);
        s.print(" ");
        methodName.decompile(s);
        s.print("(");
        params.decompile(s);
        s.print(") ");
        body.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        returnType.prettyPrint(s, prefix, false);
        methodName.prettyPrint(s, prefix, false);
        params.prettyPrint(s, prefix, false);
        body.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        returnType.iter(f);
        methodName.iter(f);
        params.iter(f);
        body.iter(f);
    }

    @Override
    public void verifyDeclMethod(DecacCompiler compiler, SymbolTable.Symbol superClass, ClassDefinition classDef) throws ContextualError {
        assert(compiler != null);
        assert(superClass != null);
        assert(classDef != null);
        assert(classDef.getMembers() != null);
        // --- Rule (2.7) conditions ---
        int index;
        // Getting the sigature of the method
        Signature sig = this.params.verifyListDeclParams(compiler);
        EnvironmentExp envExpSuper = classDef.getMembers().getParent();
        ExpDefinition superDef = envExpSuper.get(methodName.getName());
        if(superDef != null){
            // The name is defined on the superclass
            // Is it a method ? -> if yes : no problem, else (it's a field) : error.
            MethodDefinition superMethodDef = superDef.asMethodDefinition("The method " + methodName.getName() + " cannot be a field in the superclass " + superClass.getName() + ".", this.getLocation());
            index = superMethodDef.getIndex();
            if(!superMethodDef.getSignature().equals(sig)){
                // The signature isn't the same.
                throw new ContextualError("The method " + methodName.getName() + " must have the same signature as in the superclass " + superClass.getName() + ".", this.getLocation());
            }
            if(superMethodDef.getType().isClassOrNull() && superMethodDef.getType() != null){
                // Return type is an object: we verify the return type of the superclass method is a superclass of the actual return type.
                Type returnT = compiler.getEnvironmentType().get(returnType.getName()).getType();
                if(!returnT.isClass()){
                    throw new ContextualError("The return type of " + methodName.getName() + " must be a subclass of " + superMethodDef.getType().getName() + ".", this.getLocation());
                }
                ClassType returnClassType = (ClassType) returnT;
                if(!(returnT.subtype(superMethodDef.getType()))){
                    throw new ContextualError("The return type of " + methodName.getName() + " must be a subclass of " + superMethodDef.getType().getName() + ".", this.getLocation());
                }
            }
            else{
                // Return type isn't an object: we verify the return type is the same as the method defined in the superclass
                if(!compiler.getType(returnType.getName()).sameType(superMethodDef.getType())){
                    throw new ContextualError("The return type of " + methodName.getName() + " must be " + superMethodDef.getType().getName() + ".", this.getLocation());
                }
            }
        }
        else{
            classDef.incNumberOfMethods();
            index = classDef.getNumberOfMethods();
        }
        // Creating the new MethodDefinition
        Type type = compiler.getType(this.returnType.getName().getName());
        MethodDefinition methodDef = new MethodDefinition(type, this.getLocation(), sig, index);
        try{
            classDef.getMembers().declare(methodName.getName(), methodDef);
        } catch (EnvironmentExp.DoubleDefException ex){
            // --- Rule (2.6) condition ---
            throw new ContextualError("The field/method " + methodName.getName().getName() + " is already defined.", this.getLocation());
        }
        // Decorations
        TypeDefinition typeDef = compiler.getEnvironmentType().get(this.returnType.getName());
        this.returnType.setDefinition(typeDef);
        this.returnType.setType(typeDef.getType());
        this.methodName.setDefinition(methodDef);
        this.methodName.setType(typeDef.getType());
    }

    @Override
    public void verifyDeclMethodBody(DecacCompiler compiler, ClassDefinition classDef) throws ContextualError {
        Type returnT = this.returnType.getType();
        EnvironmentExp paramsEnv = this.params.verifyListDeclParamsEnv(compiler);
        this.body.verifyMethodBody(compiler, classDef, paramsEnv, returnT);
    }

    @Override
    protected void codeGenMethodTable(DecacCompiler compiler, ClassDefinition classDef, String className, int offsetClass) throws ContextualError {
        params.setOperandParams();
        MethodDefinition methodDefinition = this.methodName.getMethodDefinition();
        int index = methodDefinition.getIndex();
        try {
            Label labelMethod = new Label("code." + className + "." + this.methodName.getName());
            classDef.addMethodLabel(labelMethod, index);
            methodDefinition.setLabel(labelMethod);
            RegisterOffset addrMethod = new RegisterOffset(index + offsetClass, Register.GB);
            methodDefinition.setOperand(addrMethod);
        } catch (IllegalArgumentException e) {
            throw new ContextualError(e.getMessage(), this.getLocation());
        }
    }
    
    @Override
    protected void codeGenMethodBody(DecacCompiler compiler) {
        body.codeGenMethodBody(compiler, this.methodName.getMethodDefinition().getLabel(), this.returnType.getDefinition().getType().isVoid());
    }
    
}
