package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.BGT;
import fr.ensimag.ima.pseudocode.instructions.BLE;
import fr.ensimag.ima.pseudocode.instructions.SGT;

/**
 * Greater comparison (>).
 * @author gl38
 * @date 01/01/2020
 */
public class Greater extends AbstractOpIneq {

    public Greater(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected String getOperatorName() {
        return ">";
    }

    @Override
    protected Instruction getInstruction(GPRegister destReg) {
        return new SGT(destReg);
    }

    @Override
    protected void branch(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        if(branchIfTrue){
            compiler.addInstruction(new BGT(label));
        }
        else{
            compiler.addInstruction(new BLE(label));
        }
    }

}
