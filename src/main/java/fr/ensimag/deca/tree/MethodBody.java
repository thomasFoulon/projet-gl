/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Label;
import java.io.PrintStream;

/**
 * Method body.
 * @author gl38
 * @date 17/01/2020
 */
public class MethodBody extends AbstractMethodBody{

    final private ListDeclVar decls;
    final private ListInst insts;

    public MethodBody(ListDeclVar decls, ListInst insts) {
        this.decls = decls;
        this.insts = insts;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.println("{");
        s.indent();
        decls.decompile(s);
        insts.decompile(s);
        s.unindent();
        s.println("}");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        decls.prettyPrint(s, prefix, false);
        insts.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        decls.iter(f);
        insts.iter(f);
    }

    @Override
    public void verifyMethodBody(DecacCompiler compiler, ClassDefinition classDef, EnvironmentExp paramsEnv, Type returnT) throws ContextualError {
        paramsEnv.stacking(classDef.getMembers()); // to respect the precondition of verifyListDeclVariable()
        decls.verifyListDeclVariable(compiler, paramsEnv, classDef);
        insts.verifyListInst(compiler, paramsEnv, classDef, returnT);
    }

    @Override
    protected void codeGenMethodBody(DecacCompiler compiler, Label labelOfMethod, boolean returnIsVoid) {
        compiler.initializeMethodBody(labelOfMethod, returnIsVoid);
        decls.codeGenListDeclVar(compiler);
        insts.codeGenListInst(compiler);
        compiler.endMethodBody();
    }

}
