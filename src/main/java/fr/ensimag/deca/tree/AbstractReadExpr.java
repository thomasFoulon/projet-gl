package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.instructions.LOAD;


/**
 * read...() statement.
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractReadExpr extends AbstractExpr {

    public AbstractReadExpr() {
        super();
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        this.codeGenOperation(compiler);
        compiler.addErrorInvalidInput();
        compiler.addInstruction(new LOAD(Register.R1, compiler.getR(numRegister)));
    }
    
    /**
     * Generate the assembly code for the read operation
     * the results is stored in R1
     * @param compiler the compiler
     */
    protected abstract void codeGenOperation(DecacCompiler compiler);


}
