/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;

/**
 * Parameter declaration.
 * @author gl38
 * @date 17/01/2020
 */
public abstract class AbstractDeclParam extends Tree{

    /**
     * Implements non-terminal "decl_param" of [SyntaxeContextuelle] in pass 2
     * @param compiler contains the "env_types" attribute
     * @return the type of the parameter
     * @throws ContextualError if there is a contextual error
     */
    public abstract Type verifyDeclParam(DecacCompiler compiler) throws ContextualError;

    /**
     * Implements non-terminal "decl_param" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains the "env_types" attribute
     * @param env the environment where to add the parameter
     * @throws ContextualError if there is a contextual error
     */
    public abstract void verifyDeclParamEnv(DecacCompiler compiler, EnvironmentExp env) throws ContextualError;
    
    /**
     * Set the operand for the parameter definition.
     * @param offset the offset of the LB register.
     */
    public abstract void setOperandParam(int offset);
    
}
