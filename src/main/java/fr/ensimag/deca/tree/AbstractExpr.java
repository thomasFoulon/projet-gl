package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.WFLOAT;
import fr.ensimag.ima.pseudocode.instructions.WFLOATX;
import fr.ensimag.ima.pseudocode.instructions.WINT;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Expression, i.e. anything that has a value.
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractExpr extends AbstractInst {
    /**
     * @return true if the expression does not correspond to any concrete token
     * in the source code (and should be decompiled to the empty string).
     */
    boolean isImplicit() {
        return false;
    }

    /**
     * Get the type decoration associated to this expression (i.e. the type computed by contextual verification).
     * @return  the type decoration associated to the expression
     */
    public Type getType() {
        return type;
    }

    protected void setType(Type type) {
        Validate.notNull(type);
        this.type = type;
    }
    private Type type;

    @Override
    protected void checkDecoration() {
        if (getType() == null) {
            throw new DecacInternalError("Expression " + decompile() + " has no Type decoration");
        }
    }

    /**
     * Verify the expression for contextual error.
     *
     * implements non-terminals "expr" and "lvalue"
     *    of [SyntaxeContextuelle] in pass 3
     *
     * @param compiler  (contains the "env_types" attribute)
     * @param localEnv
     *            Environment in which the expression should be checked
     *            (corresponds to the "env_exp" attribute)
     * @param currentClass
     *            Definition of the class containing the expression
     *            (corresponds to the "class" attribute)
     *             is null in the main bloc.
     * @return the Type of the expression
     *            (corresponds to the "type" attribute)
     * @throws ContextualError if there is a contextual error
     */
    public abstract Type verifyExpr(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass)
            throws ContextualError;

    /**
     * Verify the expression in right hand-side of (implicit) assignments
     *
     * implements non-terminal "rvalue" of [SyntaxeContextuelle] in pass 3
     *
     * @param compiler  contains the "env_types" attribute
     * @param localEnv corresponds to the "env_exp" attribute
     * @param currentClass corresponds to the "class" attribute
     * @param expectedType corresponds to the "type1" attribute
     * @return this with an additional ConvFloat if needed...
     * @throws ContextualError if there is a contextual error
     */
    public AbstractExpr verifyRValue(DecacCompiler compiler,
            EnvironmentExp localEnv, ClassDefinition currentClass,
            Type expectedType)
            throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        assert(expectedType != null);
        Type t = this.verifyExpr(compiler, localEnv, currentClass);
        if(!t.sameType(expectedType)){
            if(expectedType.isFloat() && t.isInt()){
                // We call the verify() here because we need to decorate the ConvFloat node
                ConvFloat c = new ConvFloat(this);
                c.verifyExpr(compiler, localEnv, currentClass);
                return c;
            }
            else if(expectedType.isClass() && t.isClassOrNull()){
                if(t.subtype(expectedType)){
                    return this;
                }
                else throw new ContextualError("The expression must be a subtype of " + expectedType.getName() + ".", this.getLocation());
            }
            else throw new ContextualError("The expression must be of type " + expectedType.getName() + ".", this.getLocation());
        }
        else{
            this.setType(t);
            return this;
        }
    }


    @Override
    protected void verifyInst(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass, Type returnType)
            throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        assert(returnType != null);
        this.verifyExpr(compiler, localEnv, currentClass);
    }

    /**
     * Verify the expression as a condition, i.e. check that the type is
     * boolean.
     *
     * @param localEnv
     *            Environment in which the condition should be checked.
     * @param currentClass
     *            Definition of the class containing the expression, or null in
     *            the main program.
     */
    void verifyCondition(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        Type t = this.verifyExpr(compiler, localEnv, currentClass);
        if (!t.isBoolean()) {
            throw new ContextualError("Condition must be of type (boolean).", this.getLocation());
        }
        this.setType(t);
    }

    @Override
    protected void codeGenInst(DecacCompiler compiler) {
        this.codeGenExpr(compiler, 2);
    }
    
    /**
     * Generate the assembly code to store the results of the expression in the
     * register numRegister
     * @param compiler the compiler
     * @param numRegister the register where the results of the expression will be
     *                      stored
     */
    protected abstract void codeGenExpr(DecacCompiler compiler, int numRegister);

    /**
     * Get the DVal representing the expression
     * @return null if the Expr is not of immediate type
     */
    protected DVal getDVal() {
        return null;
    }
    
    @Override
    protected void decompileInst(IndentPrintStream s) {
        decompile(s);
        s.print(";");
    }

    @Override
    protected void prettyPrintType(PrintStream s, String prefix) {
        Type t = getType();
        if (t != null) {
            s.print(prefix);
            s.print("type: ");
            s.print(t);
            s.println();
        }
    }

    /**
     * Generate the assembly code to print the expression
     * @param compiler the compiler
     * @param printHex print as hexadecimal float when true
     */
    protected void codeGenPrint(DecacCompiler compiler, boolean printHex) {
        this.codeGenExpr(compiler, 1);
        if (this.getType().isFloat()) {
            if (printHex) {
                compiler.addInstruction(new WFLOATX());
            } else {
                compiler.addInstruction(new WFLOAT());                
            }
        } else if (this.getType().isInt()) {
            compiler.addInstruction(new WINT());
        }
    }
    
    /**
     * Generate the assembly code for the expression as a condition.
     * @param compiler the compiler
     * @param branchIfTrue branch on the label if the condition if true or not 
     * @param label the label where to branch
     */
    protected void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label){
        assert(this.getType().isBoolean());
        throw new UnsupportedOperationException("not supposed to be here");
    }
}
