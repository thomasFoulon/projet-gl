/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.ima.pseudocode.GPRegister;

/**
 * List of field declarations.
 * @author gl38
 * @date 01/01/2020
 */
public class ListDeclField extends TreeList<AbstractDeclField>{

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclField declField : this.getList()) {
            declField.decompile(s);
        }
    }

    /**
     * Implements non-terminal "list_decl_field" of [SyntaxeContextuelle] in pass 2
     * @param compiler contains the "env_types" attribute
     * @param superClass the symbol of the superclass
     * @param className the symbol of the class where the field is declared
     * @param classDef the class definition containing the members where to add the field definitions
     * @throws ContextualError if there is a contextual error
     */
    public void verifyListDeclField(DecacCompiler compiler, Symbol superClass, Symbol className, ClassDefinition classDef) throws ContextualError {
        assert(compiler != null);
        assert(superClass != null);
        assert(className != null);
        assert(classDef != null);
        for(AbstractDeclField f : this.getList()){
            // The potential (2.4) rule error is verified in verifyDeclField
            f.verifyDeclField(compiler, superClass, className, classDef);
        }
    }

    /**
     * Implements non-terminal "list_decl_field" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains the "env_types" attribute
     * @param classDef the class definition containing the members of the class
     * @throws ContextualError if there is a contextual error
     */
    public void verifyListDeclFieldInitialization(DecacCompiler compiler, ClassDefinition classDef) throws ContextualError {
        assert(compiler != null);
        assert(classDef != null);
        for(AbstractDeclField f : this.getList()) {
            f.verifyDeclFieldInitialization(compiler, classDef);
        }
    }

    /**
     * Generate the code 
     * @param compiler
     * @param regAddr 
     */
    void codeGenListFields(DecacCompiler compiler, GPRegister regAddr) {
        for (AbstractDeclField f : getList()) {
            f.codeGenDeclField(compiler, regAddr);
        }
    }
    
}
