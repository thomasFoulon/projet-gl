package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.codegen.RegisterManager;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Binary expressions.
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractBinaryExpr extends AbstractExpr {

    public AbstractExpr getLeftOperand() {
        return leftOperand;
    }

    public AbstractExpr getRightOperand() {
        return rightOperand;
    }

    protected void setLeftOperand(AbstractExpr leftOperand) {
        Validate.notNull(leftOperand);
        this.leftOperand = leftOperand;
    }

    protected void setRightOperand(AbstractExpr rightOperand) {
        Validate.notNull(rightOperand);
        this.rightOperand = rightOperand;
    }

    private AbstractExpr leftOperand;
    private AbstractExpr rightOperand;

    public AbstractBinaryExpr(AbstractExpr leftOperand,
            AbstractExpr rightOperand) {
        Validate.notNull(leftOperand, "left operand cannot be null");
        Validate.notNull(rightOperand, "right operand cannot be null");
        Validate.isTrue(leftOperand != rightOperand, "Sharing subtrees is forbidden");
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }


    @Override
    public void decompile(IndentPrintStream s) {
        s.print("(");
        getLeftOperand().decompile(s);
        s.print(" " + getOperatorName() + " ");
        getRightOperand().decompile(s);
        s.print(")");
    }

    abstract protected String getOperatorName();

    @Override
    protected void iterChildren(TreeFunction f) {
        leftOperand.iter(f);
        rightOperand.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        leftOperand.prettyPrint(s, prefix, false);
        rightOperand.prettyPrint(s, prefix, true);
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        DVal dvalRight = this.getRightOperand().getDVal();
        this.getLeftOperand().codeGenExpr(compiler, numRegister);
        if (dvalRight != null) {
            this.codeGenOperation(compiler, dvalRight, numRegister);
        } else {
            int numRegRight = RegisterManager.getNextRegister(compiler, numRegister);
            this.getRightOperand().codeGenExpr(compiler, numRegRight);
            numRegRight = RegisterManager.getStoredRegister(compiler, numRegister, numRegRight);
            this.codeGenOperation(compiler, numRegRight, numRegister);
        }
    }

    /**
     * Generate the assembly code for the binary operation
     * @param compiler the compiler
     * @param dvalRight the value of the right operand
     * @param numRegister the number of the register where the result will be
     *                      stored
     */
    protected abstract void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister);

    /**
     * Generate the assembly code for the binary operation
     * @param compiler the compiler
     * @param srcRegister the number of the register where the results of 
     *                      the right operand is stored
     * @param destRegister the number of the register where the results of the
     *                      left operand is stored and where the results of the
     *                      expression will be stored
     */
    protected void codeGenOperation(DecacCompiler compiler, int srcRegister, int destRegister) {
        this.codeGenOperation(compiler, compiler.getR(srcRegister), destRegister);
    }
}
