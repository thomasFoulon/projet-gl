package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.FieldDefinition;
import fr.ensimag.deca.context.MethodDefinition;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.VariableDefinition;
import fr.ensimag.deca.tools.SymbolTable;

/**
 * Identifier.
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractIdentifier extends AbstractLValue {

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * ClassDefinition.
     *
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @return the definition of the Identifier as a ClassDefinition
     */
    public abstract ClassDefinition getClassDefinition();

    public abstract Definition getDefinition();

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * FieldDefinition.
     *
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @return the definition of the Identifier as a FieldDefinition
     */
    public abstract FieldDefinition getFieldDefinition();

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * MethodDefinition.
     *
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @return the definition of the Identifier as a MethodDefinition
     */
    public abstract MethodDefinition getMethodDefinition();

    public abstract SymbolTable.Symbol getName();

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a ExpDefinition.
     *
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @return the definition of the Identifier as an ExpDefinition
     */
    public abstract ExpDefinition getExpDefinition();

    /**
     * Like {@link #getDefinition()}, but works only if the definition is a
     * VariableDefinition.
     *
     * This method essentially performs a cast, but throws an explicit exception
     * when the cast fails.
     *
     * @return the definition of the Identifier as a VariableDefinition
     */
    public abstract VariableDefinition getVariableDefinition();

    public abstract void setDefinition(Definition definition);

    /**
     * Implements non-terminal "type" of [SyntaxeContextuelle] in the 3 passes
     * @param compiler contains "env_types" attribute
     * @return the type corresponding to this identifier
     *         (corresponds to the "type" attribute)
     * @throws ContextualError if there is a contextual error
     */
    public abstract Type verifyType(DecacCompiler compiler) throws ContextualError;
    
    
    /**
     * Implements non-terminal "field_ident" of [SyntaxeContextuelle] in pass 3
     * @param members the environment of the class
     * @return the field definition
     * @throws ContextualError if there is a contextual error
     */
    public abstract FieldDefinition verifyFieldIdent(EnvironmentExp members) throws ContextualError;
    
    /**
     * Implements non-terminal "method_ident" of [SyntaxeContextuelle] in pass 3
     * @param members the environment of the class
     * @return the method definition
     * @throws ContextualError if there is a contextual error
     */
    public abstract MethodDefinition verifyMethodIdent(EnvironmentExp members) throws ContextualError;
}
