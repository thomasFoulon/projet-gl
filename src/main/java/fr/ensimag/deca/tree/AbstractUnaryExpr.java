package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DVal;
import java.io.PrintStream;
import org.apache.commons.lang.Validate;

/**
 * Unary expression.
 *
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractUnaryExpr extends AbstractExpr {

    public AbstractExpr getOperand() {
        return operand;
    }
    
    private final AbstractExpr operand;
    
    public AbstractUnaryExpr(AbstractExpr operand) {
        Validate.notNull(operand);
        this.operand = operand;
    }

    protected abstract String getOperatorName();

    @Override
    public void decompile(IndentPrintStream s) {
        if (!this.isImplicit()) {
            s.print(this.getOperatorName());            
        }
        this.operand.decompile(s);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        operand.iter(f);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        operand.prettyPrint(s, prefix, true);
    }
    
    
    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        DVal valOper = this.getOperand().getDVal();
        if (valOper == null) {
            this.getOperand().codeGenExpr(compiler, numRegister);
            valOper = compiler.getR(numRegister);
        }
        this.codeGenOperation(compiler, valOper, numRegister);
    }

    /**
     * Generate the assembly code for the unary operation
     * @param compiler the compiler
     * @param dvalRight the value of the operand
     * @param numRegister the number of the register where the result will be
     *                      stored
     */
    protected abstract void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister);
}
