/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Signature;
import fr.ensimag.deca.tools.IndentPrintStream;

/**
 * List of parameter declarations.
 * @author gl38
 * @date 01/01/2020
 */
public class ListDeclParam extends TreeList<AbstractDeclParam>{

    @Override
    public void decompile(IndentPrintStream s) {
        int n = getList().size();
        int i = 1;
        for (AbstractDeclParam declParam : this.getList()) {
            declParam.decompile(s);
            if (i < n) {
                s.print(", ");
            }
            i++;
        }
    }

    /**
     * Implements non-terminal "list_decl_param" of [SyntaxeContextuelle] in pass 2
     * @param compiler contains the "env_types" attribute
     * @return the signature of the params
     * @throws ContextualError if there is a contextual error
     */
    public Signature verifyListDeclParams(DecacCompiler compiler) throws ContextualError {
        assert(compiler != null);
        Signature sig = new Signature();
        for(AbstractDeclParam p : this.getList()){
            sig.add(p.verifyDeclParam(compiler));
        }
        return sig;
    }

    /**
     * Implements non-terminal "list_decl_param" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains the "env_types" attribute
     * @return the environment containing the params
     * @throws ContextualError if there is a contextual error
     */
    public EnvironmentExp verifyListDeclParamsEnv(DecacCompiler compiler) throws ContextualError {
        EnvironmentExp env = new EnvironmentExp(null);
        for(AbstractDeclParam p : this.getList()){
            p.verifyDeclParamEnv(compiler, env);
        }
        return env;
    }
    
    public void setOperandParams(){
        int index = -3;
        for (AbstractDeclParam p : this.getList()){
            p.setOperandParam(index);
            index--;
        }
    }
    
}
