package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.LabelOperand;
import fr.ensimag.ima.pseudocode.NullOperand;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.CMP;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.RTS;
import fr.ensimag.ima.pseudocode.instructions.SEQ;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import org.apache.log4j.Logger;

/**
 * List of class declarations.
 * @author gl38
 * @date 01/01/2020
 */
public class ListDeclClass extends TreeList<AbstractDeclClass> {

    private static final Logger LOG = Logger.getLogger(ListDeclClass.class);

    @Override
    public void decompile(IndentPrintStream s) {
        for (AbstractDeclClass c : getList()) {
            c.decompile(s);
            s.println();
        }
    }

    /**
     * Pass 1 of [SyntaxeContextuelle]
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError if there is a contextual error
     */
    public void verifyListClass(DecacCompiler compiler) throws ContextualError {
        LOG.debug("verify listClass: start");
        for(AbstractDeclClass d : this.getList()){    // (1.2)
            d.verifyClass(compiler);
        }
        LOG.debug("verify listClass: end");
    }

    /**
     * Pass 2 of [SyntaxeContextuelle]
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError if there is a contextual error
     */
    public void verifyListClassMembers(DecacCompiler compiler) throws ContextualError {
        for(AbstractDeclClass d : this.getList()){    // (2.2)
            d.verifyClassMembers(compiler);
        }
    }

    /**
     * Pass 3 of [SyntaxeContextuelle]
     * @param compiler the compiler containing the "env_types" attribute
     * @throws ContextualError if there is a contextual error
     */
    public void verifyListClassBody(DecacCompiler compiler) throws ContextualError {
        for(AbstractDeclClass d : this.getList()){    // (3.2)
            d.verifyClassBody(compiler);
        }
    }

    /**
     * Generate the code to create the method table for all the classes
     * Generate also Object with the method equals
     * @param compiler 
     */
    public void codeGenMethodTable (DecacCompiler compiler) throws ContextualError {
        this.codeGenDeclareObjectClass(compiler);
        compiler.addComment("Declare method table of other classes");
        for (AbstractDeclClass d : this.getList()){
            d.codeGenMethodTable(compiler);
        }
    }
    
    /**
     * Generate the code of the instanciation of the classes
     * and their method body
     * Generate also the method body of Object.equals
     * @param compiler 
     */
    public void codeGenListDeclMethod(DecacCompiler compiler) throws ContextualError {
        this.codeGenDeclareEqualsMethod(compiler);
        for (AbstractDeclClass d : this.getList()) {
            d.codeGenInitialization(compiler);
            d.codeGenMethodBody(compiler);
        }
    }
    
    private final Label equalsLabel = new Label("code.Object.equals");
    
    /**
     * Generate the method body of Object.equals(Object o)
     * @param compiler 
     */
    public void codeGenDeclareEqualsMethod(DecacCompiler compiler) {
        compiler.addComment("Equals method");
        compiler.addLabel(equalsLabel);
        compiler.addInstruction(new LOAD(new RegisterOffset(-3, Register.LB), Register.R0), "retrieve other object");
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), Register.R1), "retrieve this");
        compiler.addInstruction(new CMP(Register.R1, Register.R0));
        compiler.addInstruction(new SEQ(Register.R0));
        compiler.addInstruction(new RTS());
    }
    
    /**
     * Generate the code to initialize the method table with the class Object
     * @param compiler 
     */
    public void codeGenDeclareObjectClass (DecacCompiler compiler){
        assert(compiler != null);
        // Declare the variable in the stack
        ClassDefinition definitionObject = (ClassDefinition) compiler.getEnvironmentType().get(compiler.getSymbol("Object"));
        compiler.getStackManager().declareClass(definitionObject);
        definitionObject.addMethodLabel(equalsLabel.toString());
        DAddr addressObject = definitionObject.getOperand();
        // Generate the code
        compiler.addComment("Declaration of Class Object");
        compiler.addInstruction(new LOAD(new NullOperand(), Register.R0));
        compiler.addInstruction(new STORE(Register.R0, addressObject));
        compiler.addInstruction(new LOAD(new LabelOperand(equalsLabel), Register.R0));
        compiler.addInstruction(new STORE(Register.R0, new RegisterOffset(2, Register.GB)));
    }
}
