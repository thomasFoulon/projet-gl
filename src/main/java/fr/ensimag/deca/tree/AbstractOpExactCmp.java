package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Type;


/**
 * Exact comparison expressions (==, !=).
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractOpExactCmp extends AbstractOpCmp {

    public AbstractOpExactCmp(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    protected void verifyType(DecacCompiler compiler, Type tLeft, Type tRight) throws ContextualError {
        if (tLeft.isVoid() || tLeft.isString()) {
            throw new ContextualError("Left operand cannot be of type (void) or (string).", this.getLeftOperand().getLocation());
        }
        if ((tLeft.isInt() || tLeft.isFloat()) && !(tRight.isInt() || tRight.isFloat())) {
            throw new ContextualError("Right operand must be of type (int) or (float).", this.getRightOperand().getLocation());
        }
        if (tLeft.isBoolean() && !tRight.isBoolean()) {
            throw new ContextualError("Right operand must be of type (boolean).", this.getRightOperand().getLocation());
        }
        if (tLeft.isClassOrNull() && !tRight.isClassOrNull()) {
            throw new ContextualError("Right operand must be an object or null.", this.getRightOperand().getLocation());
        }
    }

}
