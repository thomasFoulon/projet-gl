/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.ExpDefinition;
import fr.ensimag.deca.context.ParamDefinition;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import java.io.PrintStream;

/**
 * Parameter declaration.
 * @author gl38
 * @date 17/01/2020
 */
public class DeclParam extends AbstractDeclParam {
    
    final private AbstractIdentifier type;
    final private AbstractIdentifier paramName;
    
    public DeclParam(AbstractIdentifier type, AbstractIdentifier paramName) {
        this.type = type;
        this.paramName = paramName;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        type.decompile(s);
        s.print(" ");
        paramName.decompile(s);
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        type.prettyPrint(s, prefix, false);
        paramName.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        type.iter(f);
        paramName.iter(f);
    }

    @Override
    public Type verifyDeclParam(DecacCompiler compiler) throws ContextualError {
        assert(compiler != null);
        Type type = compiler.getType(this.type.getName().getName());
        if(type.isVoid()){
            throw new ContextualError("A parameter cannot be of type void.", this.getLocation());
        }
        // Decorations
        this.type.setDefinition(compiler.getEnvironmentType().get(this.type.getName()));
        this.type.setType(type);
        return type;
    }

    @Override
    public void verifyDeclParamEnv(DecacCompiler compiler, EnvironmentExp env) throws ContextualError {
        ExpDefinition paramDef;
        try {
            assert(compiler != null);
            assert(env != null);
            paramDef = new ParamDefinition(type.getType(), this.getLocation());
            env.declare(paramName.getName(), paramDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            throw new ContextualError("The parameter " + paramName.getName() + " is already used.", paramName.getLocation());
        }
        // Decorations
        this.paramName.setDefinition(paramDef);
    }

    @Override
    public void setOperandParam(int offset) {
        ParamDefinition def = (ParamDefinition)this.paramName.getDefinition();
        def.setOffset(offset);
        def.setOperand(new RegisterOffset(offset, Register.LB));
    }
    
}
