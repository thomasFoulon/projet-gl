package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.ClassType;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.Definition;
import fr.ensimag.deca.context.EnvironmentType;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.LabelOperand;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.BRA;
import fr.ensimag.ima.pseudocode.instructions.BSR;
import fr.ensimag.ima.pseudocode.instructions.LEA;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import fr.ensimag.ima.pseudocode.instructions.STORE;
import java.io.PrintStream;

/**
 * Declaration of a class (<code>class name extends superClass {members}<code>).
 * 
 * @author gl38
 * @date 01/01/2020
 */
public class DeclClass extends AbstractDeclClass {
    
    final private AbstractIdentifier className;
    final private AbstractIdentifier superClass;
    final private ListDeclField fields;
    final private ListDeclMethod methods;
    
    public DeclClass(AbstractIdentifier className, AbstractIdentifier superClass,
                     ListDeclField fields, ListDeclMethod methods) {
        this.className = className;
        this.superClass = superClass;
        this.fields = fields;
        this.methods = methods;
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("class ");
        className.decompile(s);
        s.print(" extends ");
        superClass.decompile(s);
        s.println(" {");
        s.indent();
        fields.decompile(s);
        methods.decompile(s);
        s.unindent();
        s.print("}");
    }

    @Override
    protected void verifyClass(DecacCompiler compiler) throws ContextualError {
        assert(compiler != null);
        // Verifying the superclass is well defined.
        Definition superDef = compiler.getEnvironmentType().get(superClass.getName());
        if(superDef == null){
            throw new ContextualError("The superclass " + superClass.getName() + " is not defined.", superClass.getLocation());
        }
        // Verifying the superclass specified is actually a class.
        if(!superDef.isClass()){
            throw new ContextualError(superClass.getName() + " is not a class.", superClass.getLocation());
        }
        ClassDefinition superClassDef = (ClassDefinition) superDef;
        // Creating the new ClassDefinition
        ClassType classType = new ClassType(className.getName(), this.getLocation(), superClassDef);
        ClassDefinition classDef = new ClassDefinition(classType, this.getLocation(), superClassDef);
        try {
            // Declaration of the class in the predefined environment
            compiler.getEnvironmentType().declare(className.getName(), classDef);
        } catch (EnvironmentType.DoubleDefException ex) {
            // The class is already defined of is a scalar type.
            Definition def = compiler.getEnvironmentType().get(className.getName());
            if(def.isClass()) throw new ContextualError("Class " + className.getName() + " is already defined.", this.getLocation());
            else throw new ContextualError("Cannot declare a class of name " + className.getName() + ".", this.getLocation());
        }
        superClass.setDefinition(superClassDef);
        superClass.setType(superClassDef.getType());
        className.setDefinition(classDef);
        className.setType(classType);
    }

    @Override
    protected void verifyClassMembers(DecacCompiler compiler)
            throws ContextualError {
        assert(compiler != null);
        ClassDefinition superClassDef = superClass.getClassDefinition(); // should not give an error (pass 1)
        ClassDefinition classDef = className.getClassDefinition(); // should work because of the pass 1
        classDef.setNumberOfFields(superClassDef.getNumberOfFields());
        classDef.setNumberOfMethods(superClassDef.getNumberOfMethods());
        fields.verifyListDeclField(compiler, superClass.getName(), className.getName(), classDef);
        if (this.className.getClassDefinition().getNumberOfFields() > 0) {
            try {
                new Label("fin.code.init." + this.className.getName().toString());
            } catch (IllegalArgumentException e) {
                throw new ContextualError("Creating init label for the class : " + e.getMessage(), this.getLocation());
            }
        }
        methods.verifyListDeclMethod(compiler, superClass.getName(), classDef);
    }
    
    @Override
    protected void verifyClassBody(DecacCompiler compiler) throws ContextualError {
        assert(compiler != null);
        ClassDefinition classDef = className.getClassDefinition(); // should work because of the pass 1
        fields.verifyListDeclFieldInitialization(compiler, classDef);
        methods.verifyListDeclMethodBody(compiler, classDef);
    }


    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        className.prettyPrint(s, prefix, false);
        superClass.prettyPrint(s, prefix, false);
        fields.prettyPrint(s, prefix, false);
        methods.prettyPrint(s, prefix, true);
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        className.iter(f);
        superClass.iter(f);
        fields.iter(f);
        methods.iter(f);
    }
    
    @Override
    protected void codeGenMethodTable(DecacCompiler compiler) throws ContextualError {
        compiler.addComment("Method table for class " + this.className.getName());
        ClassDefinition classDef = this.className.getClassDefinition();
        classDef.initMethodLabel(superClass.getClassDefinition());
        int offsetClass = compiler.getStackManager().declareClass(classDef);
        // Add adress of super class method table
        DAddr addrSuperClass = classDef.getSuperClass().getOperand();
        compiler.addInstruction(new LEA(addrSuperClass, Register.R0));
        compiler.addInstruction(new STORE(Register.R0, classDef.getOperand()));
        // Add all method in an ArrayList
        this.methods.codeGenMethodTable(compiler, classDef, this.className.getName().getName(), offsetClass);
        // Generate the code for the method table
        int index = 1;
        for (Label methodLabel : classDef.getMethodsLabels()) {
            RegisterOffset operand = new RegisterOffset(index + offsetClass, Register.GB);
            compiler.addInstruction(new LOAD(new LabelOperand(methodLabel), Register.R0));
            compiler.addInstruction(new STORE(Register.R0, operand));
            index++;
        }
    }
    
    @Override
    protected void codeGenInitialization(DecacCompiler compiler) throws ContextualError {
        compiler.addComment(" ---- Class " + this.className.getName());
        if (this.className.getClassDefinition().getNumberOfFields() > 0) {
            compiler.initializeMethodBody("init." + this.className.getName());
            compiler.addComment("Initialization of class " + this.className.getName().toString());
            GPRegister regAddr = compiler.getR(2);
            compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), regAddr));
            String superClassName = this.superClass.getClassDefinition().getType().getName().toString();
            if (!superClassName.equals("Object") && this.superClass.getClassDefinition().getNumberOfFields() > 0) {
                compiler.getStackManager().pushParamForFunctionCall(regAddr);
                try {
                    Label labelInit = new Label("code.init." + superClassName);
                    compiler.addInstruction(new BSR(labelInit));
                } catch (IllegalArgumentException e) {
                    throw new ContextualError("Impossible to create initialisation label : " + e.getMessage(), this.getLocation());
                }
                compiler.getStackManager().popParamForFunctionCall(regAddr);
            }
            this.fields.codeGenListFields(compiler, regAddr);
            try {
                compiler.addInstruction(new BRA(new Label("fin.code.init." + this.className.getName().toString())));
            } catch (IllegalArgumentException e) {
                throw new ContextualError("Impossible to create initialisation label : " + e.getMessage(), this.getLocation());
            }
            compiler.endMethodBody();
        }
    }

    @Override
    protected void codeGenMethodBody(DecacCompiler compiler) {
        this.methods.codeGenMethodBody(compiler);
    }

}
