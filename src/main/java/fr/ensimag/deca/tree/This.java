/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.tools.IndentPrintStream;
import fr.ensimag.ima.pseudocode.Register;
import fr.ensimag.ima.pseudocode.RegisterOffset;
import fr.ensimag.ima.pseudocode.instructions.LOAD;
import java.io.PrintStream;

/**
 * This statement.
 * @author gl38
 * @date 17/01/2020
 */
public class This extends AbstractExpr {

    private final boolean isImplicit;
    
    public This(boolean isImplicit) {
        this.isImplicit = isImplicit;
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv, ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        if(currentClass == null){
            throw new ContextualError("this cannot be used in the main program.", this.getLocation());
        }
        // Decoration
        this.setType(currentClass.getType());
        return currentClass.getType();
    }

    @Override
    protected void codeGenExpr(DecacCompiler compiler, int numRegister) {
        compiler.addInstruction(new LOAD(new RegisterOffset(-2, Register.LB), compiler.getR(numRegister)));
    }

    @Override
    public void decompile(IndentPrintStream s) {
        s.print("this");
    }

    @Override
    protected void prettyPrintChildren(PrintStream s, String prefix) {
        // nothing to do
    }

    @Override
    protected void iterChildren(TreeFunction f) {
        // nothing to do
    }

}
