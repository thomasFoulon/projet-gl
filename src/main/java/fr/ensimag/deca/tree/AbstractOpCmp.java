package fr.ensimag.deca.tree;

import fr.ensimag.deca.context.Type;
import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.ima.pseudocode.DVal;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.instructions.CMP;

/**
 * Comparison expressions (<, >, <=, >=, ==, !=).
 * @author gl38
 * @date 01/01/2020
 */
public abstract class AbstractOpCmp extends AbstractBinaryExpr {

    public AbstractOpCmp(AbstractExpr leftOperand, AbstractExpr rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Type verifyExpr(DecacCompiler compiler, EnvironmentExp localEnv,
            ClassDefinition currentClass) throws ContextualError {
        assert(compiler != null);
        assert(localEnv != null);
        Type tLeft = this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        Type tRight = this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        this.verifyType(compiler, tLeft, tRight);
        if(tLeft.isInt() && tRight.isFloat()){
            this.setLeftOperand(new ConvFloat(this.getLeftOperand()));
            this.getLeftOperand().verifyExpr(compiler, localEnv, currentClass);
        }
        else if(tLeft.isFloat() && tRight.isInt()){
            this.setRightOperand(new ConvFloat(this.getRightOperand()));
            this.getRightOperand().verifyExpr(compiler, localEnv, currentClass);
        }
        this.setType(compiler.getType("boolean"));
        return this.getType();
    }

    @Override
    protected void codeGenOperation(DecacCompiler compiler, DVal dvalRight, int numRegister) {
        GPRegister destReg = compiler.getR(numRegister);
        compiler.addInstruction(new CMP(dvalRight, compiler.getR(numRegister)));
        compiler.addInstruction(this.getInstruction(destReg));
    }

    /**
     * Get the assembly instruction to store the result of comparison in a register
     * @param destReg the destination register
     * @return the instruction created
     */
    protected abstract Instruction getInstruction(GPRegister destReg);
    
    @Override
    protected final void codeGenCondition(DecacCompiler compiler, boolean branchIfTrue, Label label) {
        assert(compiler != null);
        assert(label != null);
        this.getLeftOperand().codeGenExpr(compiler, 2);
        this.getRightOperand().codeGenExpr(compiler, 3);
        compiler.addInstruction(new CMP(compiler.getR(3), compiler.getR(2)));
        branch(compiler, branchIfTrue, label);
    }
    
    /**
     * Generate the assembly code to branch to the label if the comparison is done
     * @param compiler
     * @param branchIfTrue boolean that indicate if the jump is done if the comparison
     *                  is true or not
     * @param label label of the jump
     */
    protected abstract void branch(DecacCompiler compiler, boolean branchIfTrue, Label label);

    /**
     * Verify the type of the operands.
     * @param compiler the compiler
     * @param tLeft the type of the left operand
     * @param tRight the type of the right operand
     * @throws ContextualError if there is a contextual error
     */
    protected abstract void verifyType(DecacCompiler compiler, Type tLeft, Type tRight) throws ContextualError;        

}
