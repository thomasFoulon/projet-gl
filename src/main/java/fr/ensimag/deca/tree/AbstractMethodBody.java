/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.tree;

import fr.ensimag.deca.DecacCompiler;
import fr.ensimag.deca.context.ClassDefinition;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.context.EnvironmentExp;
import fr.ensimag.deca.context.Type;
import fr.ensimag.ima.pseudocode.Label;

/**
 * Body of a method.
 * @author gl38
 * @date 17/01/2020
 */
public abstract class AbstractMethodBody extends Tree{

    /**
     * Implements non-terminal "method_body" of [SyntaxeContextuelle] in pass 3
     * @param compiler contains the "env_types" attribute
     * @param classDef contains the "env_exp" attribute
     * @param paramsEnv the "env_exp_params" attribute
     * @param returnT the expected return type
     * @throws ContextualError if there is a contextual error
     */
    public abstract void verifyMethodBody(DecacCompiler compiler, ClassDefinition classDef, EnvironmentExp paramsEnv, Type returnT) throws ContextualError;

    /**
     * Generate the code for the method body with the stack management and
     * registers save
     * @param compiler
     * @param labelOfMethod label of the current method
     * @param returnIsVoid true if no argument are returned (void)
     *                      (used to throw an execution error if no return was done
     *                      when an argument is needed)
     */
    protected abstract void codeGenMethodBody(DecacCompiler compiler, Label labelOfMethod, boolean returnIsVoid);
}
