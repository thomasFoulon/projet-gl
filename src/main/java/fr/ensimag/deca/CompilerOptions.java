package fr.ensimag.deca;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * User-specified options influencing the compilation.
 *
 * @author gl38
 * @date 01/01/2020
 */
public class CompilerOptions {

    public static final int QUIET = 0;
    public static final int INFO  = 1;
    public static final int DEBUG = 2;
    public static final int TRACE = 3;

    public int getDebug() {
        return debug;
    }

    public boolean getParallel() {
        return parallel;
    }

    public boolean getPrintBanner() {
        return printBanner;
    }

    public List<File> getSourceFiles() {
        return Collections.unmodifiableList(sourceFiles);
    }

    private int debug = 0;
    private boolean parallel = false;
    private boolean printBanner = false;
    private final List<File> sourceFiles = new ArrayList<>();

    public boolean getVerification() {
        return verification;
    }

    public boolean getParse() {
        return parse;
    }

    public boolean getNoCheck() {
        return noCheck;
    }

    public int getRegisters() {
        return registers;
    }

    private boolean verification = false;
    private boolean parse = false;
    private boolean noCheck = false;
    private int registers = 15;

    private void setOptions(String[] args) throws CLIException {
        boolean expectedNbReg = false;
        for (String arg : args) {
            if (expectedNbReg) {
                try {
                    int nbReg = Integer.parseInt(arg);
                    expectedNbReg = false;
                    if (nbReg < 4 || nbReg > 16) {
                        throw new CLIException("register number must be an integer between 4 and 16 given : " + nbReg);
                    }
                    registers = nbReg - 1;
                } catch (NumberFormatException e) {
                    throw new CLIException("-r X : X must be an integer given : " + arg);
                }
            } else {
                switch (arg) {
                case "-b":
                    if (args.length != 1) {
                        throw new CLIException("-b needs to be the only argument");
                    }
                    this.printBanner = true;
                    break;
                case "-p":
                    if (verification) {
                        throw new CLIException("error on argument -p : -v already specified");
                    }
                    this.parse = true;
                    break;
                case "-v":
                    if (parse) {
                        throw new CLIException("error on argument -v : -p already specified");
                    }
                    this.verification = true;
                    break;
                case "-n" :
                    this.noCheck = true;
                    break;
                case "-d":
                    this.debug++;
                    break;
                case "-r":
                    expectedNbReg = true;
                    break;
                case "-P":
                    parallel = true;
                    break;
                default :
                    if (!arg.matches("(.*).deca")) {
                        throw new CLIException("unrecognized argument : " + arg);
                    } else {
                        File source = new File(arg);
                        if (source.exists()) {
                            if (!this.sourceFiles.contains(source)) {
                                this.sourceFiles.add(source);
                            }
                        } else {
                            throw new CLIException(arg + " : no such file exists");
                        }
                    }
                    break;
                }
            }
        }        
    }

    public void parseArgs(String[] args) throws CLIException {
        setOptions(args);
        Logger logger = Logger.getRootLogger();
        // map command-line debug option to log4j's level.
        switch (getDebug()) {
        case QUIET: break; // keep default
        case INFO:
            logger.setLevel(Level.INFO); break;
        case DEBUG:
            logger.setLevel(Level.DEBUG); break;
        case TRACE:
            logger.setLevel(Level.TRACE); break;
        default:
            logger.setLevel(Level.ALL); break;
        }
        logger.info("Application-wide trace level set to " + logger.getLevel());

        boolean assertsEnabled = false;
        assert assertsEnabled = true; // Intentional side effect!!!
        if (assertsEnabled) {
            logger.info("Java assertions enabled");
        } else {
            logger.info("Java assertions disabled");
        }
    }

    protected void displayUsage() {
        System.out.println("Usage : decac [[-p | -v] [-n] [-r X] [-d]* [-P] <fichier deca>...] | [-b]");
    }

}
