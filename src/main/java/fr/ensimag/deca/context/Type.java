package fr.ensimag.deca.context;

import fr.ensimag.deca.context.ClassType;
import fr.ensimag.deca.context.ContextualError;
import fr.ensimag.deca.tools.SymbolTable.Symbol;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.deca.DecacCompiler;

/**
 * Deca Type (internal representation of the compiler)
 *
 * @author gl38
 * @date 01/01/2020
 */

public abstract class Type {


    /**
     * True if this and otherType represent the same type (in the case of
     * classes, this means they represent the same class).
     */
    public abstract boolean sameType(Type otherType);

    private final Symbol name;

    public Type(Symbol name) {
        this.name = name;
    }

    public Symbol getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName().toString();
    }

    public boolean isClass() {
        return false;
    }

    public boolean isInt() {
        return false;
    }

    public boolean isFloat() {
        return false;
    }

    public boolean isBoolean() {
        return false;
    }

    public boolean isVoid() {
        return false;
    }

    public boolean isString() {
        return false;
    }

    public boolean isNull() {
        return false;
    }

    public boolean isClassOrNull() {
        return false;
    }

    public boolean castCompatible(DecacCompiler compiler, Type other){
        return !(this.isVoid()) && (assignCompatible(compiler, other) || other.assignCompatible(compiler, this));
    }

    public boolean assignCompatible(DecacCompiler compiler, Type other){
        return (this.isFloat() && other.isInt()) || other.subtype(this);
    }

    /**
     * Is the actual type a subtype of another type ?
     * @param other the other type
     * @return true if the actual type is a subtype of another type, else false
     */
    public boolean subtype(Type other){
        if(this.isNull()) return true;
        if (this.sameType(other)) return true;
        if(this.isClass() && other.isClass()){
            ClassType thisClassType = (ClassType) this;
            ClassType otherClassType = (ClassType) other;
            if(thisClassType.isSubClassOf(otherClassType)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the same object, as type ClassType, if possible. Throws
     * ContextualError(errorMessage, l) otherwise.
     *
     * Can be seen as a cast, but throws an explicit contextual error when the
     * cast fails.
     */
    public ClassType asClassType(String errorMessage, Location l)
            throws ContextualError {
        throw new ContextualError(errorMessage, l);
    }

}
