package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable.Symbol;
import java.util.HashMap;

/**
 * Dictionary associating identifier's TypeDefinition to their names.
 *
 * This is actually a linked list of dictionaries: each EnvironmentExp has a
 * pointer to a parentEnvironment, corresponding to superblock (eg superclass).
 *
 * The dictionary at the head of this list thus corresponds to the "current"
 * block (eg class).
 *
 * Searching a definition (through method get) is done in the "current"
 * dictionary and in the parentEnvironment if it fails.
 *
 * Insertion (through method declare) is always done in the "current" dictionary.
 *
 * @author gl38
 * @date 08/01/2020
 */
public class EnvironmentType {

    private final HashMap<Symbol, TypeDefinition> environment;

    public EnvironmentType() {
        this.environment = new HashMap<>();
    }

    public HashMap<Symbol, TypeDefinition> getEnvironment(){
        return this.environment;
    }

    public static class DoubleDefException extends Exception {
        private static final long serialVersionUID = -2733379901827316441L;
    }

    /**
     * Return the definition of the symbol in the environment, or null if the
     * symbol is undefined.
     * @param key the symbol to get the environment
     * @return the environment associated with the key
     */
    public TypeDefinition get(Symbol key) {
        return this.environment.get(key);
    }

    /**
     * Add the definition def associated to the symbol name in the environment.
     *
     * Adding a symbol which is already defined in the environment,
     * - throws DoubleDefException if the symbol is in the "current" dictionary
     * - or, hides the previous declaration otherwise.
     *
     * @param name
     *            Name of the symbol to define
     * @param def
     *            Definition of the symbol
     * @throws DoubleDefException
     *             if the symbol is already defined at the "current" dictionary
     *
     */
    public void declare(Symbol name, TypeDefinition def) throws DoubleDefException {
        if(this.environment.containsKey(name)){
            throw new DoubleDefException();
        }
        else{
            this.environment.put(name, def);
        }
    }

}
