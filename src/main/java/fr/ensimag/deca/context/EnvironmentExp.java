package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable.Symbol;
import java.util.HashMap;

/**
 * Dictionary associating identifier's ExpDefinition to their names.
 *
 * This is actually a linked list of dictionaries: each EnvironmentExp has a
 * pointer to a parentEnvironment, corresponding to superblock (eg superclass).
 *
 * The dictionary at the head of this list thus corresponds to the "current"
 * block (eg class).
 *
 * Searching a definition (through method get) is done in the "current"
 * dictionary and in the parentEnvironment if it fails.
 *
 * Insertion (through method declare) is always done in the "current" dictionary.
 *
 * @author gl38
 * @date 01/01/2020
 */
public class EnvironmentExp {

    private EnvironmentExp parentEnvironment;
    private final HashMap<Symbol, ExpDefinition> environment;

    public EnvironmentExp(EnvironmentExp parentEnvironment) {
        this.parentEnvironment = parentEnvironment;
        this.environment = new HashMap<>();
    }

    public EnvironmentExp getParent(){
        return this.parentEnvironment;
    }

    public static class DoubleDefException extends Exception {
        private static final long serialVersionUID = -2733379901827316441L;
    }

    /**
     * Return the definition of the symbol in the environment, or null if the
     * symbol is undefined.
     * @param key the symbol to get the definition
     * @return the definition associated with the symbol
     */
    public ExpDefinition get(Symbol key) {
        if(this.environment.get(key) == null){
            if(this.parentEnvironment != null){
                return this.parentEnvironment.get(key);
            }
            else{
                return null;
            }
        }
        return this.environment.get(key);
    }

    /**
     * Environment stacking.
     * @param env2 the environment to stack on the current one
    **/
    public void stacking(EnvironmentExp env2){
        this.parentEnvironment = env2;
    }

    /**
     * Add the definition def associated to the symbol name in the environment.
     *
     * Adding a symbol which is already defined in the environment,
     * - throws DoubleDefException if the symbol is in the "current" dictionary
     * - or, hides the previous declaration otherwise.
     *
     * @param name
     *            Name of the symbol to define
     * @param def
     *            Definition of the symbol
     * @throws DoubleDefException
     *             if the symbol is already defined at the "current" dictionary
     *
     */
    public void declare(Symbol name, ExpDefinition def) throws DoubleDefException {
        if(this.environment.containsKey(name)){
            throw new DoubleDefException();
        }
        else{
            this.environment.put(name, def);
        }
    }

}
