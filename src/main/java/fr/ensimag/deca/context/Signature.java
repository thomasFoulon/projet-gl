package fr.ensimag.deca.context;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Signature of a method (i.e. list of arguments)
 *
 * @author gl38
 * @date 01/01/2020
 */
public class Signature implements Iterable<Type>{
    List<Type> args = new ArrayList<Type>();

    public void add(Type t) {
        args.add(t);
    }
    
    public Type paramNumber(int n) {
        return args.get(n);
    }
    
    public int size() {
        return args.size();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;
        if(!(obj instanceof Signature)) return false;
        Signature sig2 = (Signature) obj;
        Iterator<Type> i1 = args.iterator();
        Iterator<Type> i2 = sig2.args.iterator();
        while(i1.hasNext() && i2.hasNext()){
            if(!i1.next().sameType(i2.next())) return false;
        }
        if(i1.hasNext() || i2.hasNext()) return false;
        else return true;
    }

    @Override
    public Iterator<Type> iterator() {
        return args.iterator();
    }
    
    

}
