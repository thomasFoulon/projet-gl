package fr.ensimag.deca.context;

import fr.ensimag.deca.tree.Location;
import fr.ensimag.ima.pseudocode.DAddr;
import fr.ensimag.ima.pseudocode.Label;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.Validate;

/**
 * Definition of a class.
 *
 * @author gl38
 * @date 01/01/2020
 */
public class ClassDefinition extends TypeDefinition {
    
    private DAddr operand;


    public void setNumberOfFields(int numberOfFields) {
        this.numberOfFields = numberOfFields;
    }

    public int getNumberOfFields() {
        return numberOfFields;
    }

    public void incNumberOfFields() {
        this.numberOfFields++;
    }

    public int getNumberOfMethods() {
        return numberOfMethods;
    }

    public void setNumberOfMethods(int n) {
        Validate.isTrue(n >= 0);
        numberOfMethods = n;
    }

    public int incNumberOfMethods() {
        numberOfMethods++;
        return numberOfMethods;
    }

    private int numberOfFields = 0;
    private int numberOfMethods = 0;

    @Override
    public boolean isClass() {
        return true;
    }

    @Override
    public ClassType getType() {
        // Cast succeeds by construction because the type has been correctly set
        // in the constructor.
        return (ClassType) super.getType();
    };

    public ClassDefinition getSuperClass() {
        return superClass;
    }

    private final EnvironmentExp members;
    private final ClassDefinition superClass;
    private List<Label> methodsLabels;
    
    public List<Label> getMethodsLabels() {
        return this.methodsLabels;
    }

    public EnvironmentExp getMembers() {
        return members;
    }
    
    public ClassDefinition(ClassType type, Location location, ClassDefinition superClass) {
        super(type, location);
        EnvironmentExp parent;
        this.methodsLabels = new ArrayList<>();
        if (superClass != null) {
            parent = superClass.getMembers();
        } else {
            parent = null;
            
        }
        members = new EnvironmentExp(parent);
        this.superClass = superClass;
    }
    
    public ClassDefinition(ClassType type, Location location, EnvironmentExp members) {
        super(type, location);
        this.members = members;
        this.methodsLabels = new ArrayList<>();
        this.superClass = null;
    }
    
    public void addMethodLabel(Label labelMethod, int index) {        
        if (index - 1 < methodsLabels.size()) {
            methodsLabels.remove(index - 1);
            methodsLabels.add(index - 1, labelMethod);
        } else {
            methodsLabels.add(labelMethod);
        }
    }
    
    public void addMethodLabel(String nameOfMethod) {
        this.methodsLabels.add(new Label(nameOfMethod));
    }
    
    public void initMethodLabel(ClassDefinition superClass) {
        this.methodsLabels.addAll(superClass.getMethodsLabels());
    }
    
    public void setOperand (DAddr add){
        this.operand = add;
    }

    public DAddr getOperand (){
        return this.operand;
    }
    
}
