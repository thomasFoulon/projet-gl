package fr.ensimag.deca;

import fr.ensimag.deca.codegen.ErrorManager;
import fr.ensimag.deca.codegen.LabelManager;
import fr.ensimag.deca.codegen.MethodBodyManager;
import fr.ensimag.deca.codegen.StackManager;
import fr.ensimag.deca.context.*;
import fr.ensimag.deca.syntax.DecaLexer;
import fr.ensimag.deca.syntax.DecaParser;
import fr.ensimag.deca.tools.DecacInternalError;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.AbstractProgram;
import fr.ensimag.deca.tree.Location;
import fr.ensimag.deca.tree.LocationException;
import fr.ensimag.ima.pseudocode.AbstractLine;
import fr.ensimag.ima.pseudocode.GPRegister;
import fr.ensimag.ima.pseudocode.IMAProgram;
import fr.ensimag.ima.pseudocode.Instruction;
import fr.ensimag.ima.pseudocode.Label;
import fr.ensimag.ima.pseudocode.Register;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.log4j.Logger;

/**
 * Decac compiler instance.
 *
 * This class is to be instantiated once per source file to be compiled. It
 * contains the meta-data used for compiling (source file name, compilation
 * options) and the necessary utilities for compilation (symbol tables, abstract
 * representation of target file, ...).
 *
 * It contains several objects specialized for different tasks. Delegate methods
 * are used to simplify the code of the caller (e.g. call
 * compiler.addInstruction() instead of compiler.getProgram().addInstruction()).
 *
 * @author gl38
 * @date 01/01/2020
 */
public class DecacCompiler {
    private static final Logger LOG = Logger.getLogger(DecacCompiler.class);

    /**
     * Portable newline character.
     */
    private static final String nl = System.getProperty("line.separator", "\n");

    public DecacCompiler(CompilerOptions compilerOptions, File source) {
        super();
        this.compilerOptions = compilerOptions;
        this.source = source;
        this.environmentType = new EnvironmentType();
        this.symbolTable = new SymbolTable();
        this.stackManager = new StackManager(this.program);
        this.labelManager = new LabelManager();
        this.errorManager = new ErrorManager(this);
        this.methodBody = null;
	this.initializeEnvironment();
    }
    
    private MethodBodyManager methodBody;
    
    /**
     * Initialize a new MethodBodyManager
     * All new instruction will be added to the program in the methodBodyManager
     * instead of the program of this decaCompiler until endMethodBody() is called
     * @param nameOfMethod the name of the method
     */
    public void initializeMethodBody(String nameOfMethod) {
        this.methodBody = new MethodBodyManager(new Label("code." + nameOfMethod), false);
    }
    
    /**
     * Initialize a new MethodBodyManager
     * All new instruction will be added to the program in the methodBodyManager
     * instead of the program of this decaCompiler until endMethodBody() is called
     * @param labelOfMethod the label representing the name of the method
     * @param returnIsVoid true if the method return a void
     *                  (used to check if the return instruction is called on non-void method)
     */
    public void initializeMethodBody(Label labelOfMethod, boolean returnIsVoid) {
        this.methodBody = new MethodBodyManager(labelOfMethod, returnIsVoid);
        
    }

    /**
     * End the current MethodBodyManager by adding the generate code to preserve
     * registers and manage the method's stack
     * It then append the completed method body to the program of the compiler
     * All new instructions are added to the program and not to the method body
     */
    public void endMethodBody() {
        this.methodBody.endMethodBody(this);
        this.program.append(this.methodBody.getProgram());
        this.methodBody = null;
    }
    
    public GPRegister getR(int i) {
        if (this.methodBody != null) {
            this.methodBody.getStackManager().setNumHigherRegister(i);
        }
        return Register.getR(i);
    }
    
    private EnvironmentType environmentType;

    public EnvironmentType getEnvironmentType(){
        return this.environmentType;
    }

    public Type getType(String name) {
        SymbolTable.Symbol typeSymbol = this.getSymbol(name);
        Type t = this.getEnvironmentType().get(typeSymbol).getType();
        return t;
    }
    
    public Type getType(SymbolTable.Symbol symbol) {
        Type t = this.getEnvironmentType().get(symbol).getType();
        return t;
    }

    private SymbolTable symbolTable;

    public SymbolTable.Symbol getSymbol(String name){
        return this.symbolTable.create(name);
    }
    
    private StackManager stackManager;
    
    public StackManager getStackManager(){
        if (this.methodBody != null) {
            return this.methodBody.getStackManager();
        }
        return this.stackManager;
    }
    
    private LabelManager labelManager;
    
    public LabelManager getLabelManager(){
        return this.labelManager;
    }
    
    private ErrorManager errorManager;
    
    public ErrorManager getErrorManager(){
        return this.errorManager;
    }

    public MethodBodyManager getMethodBodyManager(){
        return this.methodBody;
    }
    
    private void initializeEnvironment(){
        SymbolTable.Symbol voidS, booleanS, intS, floatS, objectS;
        // Type symbols of the Deca language        
        objectS = this.symbolTable.create("Object");
        voidS = this.symbolTable.create("void");
        booleanS = this.symbolTable.create("boolean");
        intS = this.symbolTable.create("int");
        floatS = this.symbolTable.create("float");
        
        try {
            // Setup the environment
            this.environmentType.declare(voidS, new TypeDefinition(
                    new VoidType(voidS),
                    Location.BUILTIN
            ));
            this.environmentType.declare(booleanS, new TypeDefinition(
                    new BooleanType(booleanS),
                    Location.BUILTIN
            ));
            this.environmentType.declare(intS, new TypeDefinition(
                    new IntType(intS),
                    Location.BUILTIN
            ));
            this.environmentType.declare(floatS, new TypeDefinition(
                    new FloatType(floatS),
                    Location.BUILTIN
            ));
            
            Type objectT = new ClassType(objectS, Location.BUILTIN, null);
            EnvironmentExp envExpObject = new EnvironmentExp(null);
            Signature equalsSig = new Signature();
            equalsSig.add(objectT);
            try {
                envExpObject.declare(
                        this.symbolTable.create("equals"),
                        new MethodDefinition(
                                this.getType("boolean"),
                                Location.BUILTIN,
                                equalsSig,
                                1
                        )
                );
            } catch (EnvironmentExp.DoubleDefException ex) {
                java.util.logging.Logger.getLogger(DecacCompiler.class.getName()).log(Level.SEVERE, null, ex);
            }
            ClassDefinition definitionObject = new ClassDefinition(
                    new ClassType(objectS, Location.BUILTIN, null),
                    Location.BUILTIN,
                    envExpObject
            );
            definitionObject.incNumberOfMethods();
            this.environmentType.declare(objectS, definitionObject);
            
        } catch (EnvironmentType.DoubleDefException ex) {
            java.util.logging.Logger.getLogger(DecacCompiler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Source file associated with this compiler instance.
     */
    public File getSource() {
        return source;
    }

    /**
     * Compilation options (e.g. when to stop compilation, number of registers
     * to use, ...).
     */
    public CompilerOptions getCompilerOptions() {
        return compilerOptions;
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#add(fr.ensimag.ima.pseudocode.AbstractLine)
     */
    public void add(AbstractLine line) {
        if (this.methodBody != null) {
            this.methodBody.add(line);
        } else {
            program.add(line);
        }
    }

    /**
     * @see fr.ensimag.ima.pseudocode.IMAProgram#addComment(java.lang.String)
     */
    public void addComment(String comment) {
        if (this.methodBody != null) {
            this.methodBody.addComment(comment);
        } else {
            program.addComment(comment);
        }
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addLabel(fr.ensimag.ima.pseudocode.Label)
     */
    public void addLabel(Label label) {
        if (this.methodBody != null) {
            this.methodBody.addLabel(label);
        } else {
            program.addLabel(label);
        }
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag.ima.pseudocode.Instruction)
     */
    public void addInstruction(Instruction instruction) {        
        if (this.methodBody != null) {
            this.methodBody.addInstruction(instruction);
        } else {
            program.addInstruction(instruction);
        }
    }

    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#addInstruction(fr.ensimag.ima.pseudocode.Instruction,
     * java.lang.String)
     */
    public void addInstruction(Instruction instruction, String comment) {        
        if (this.methodBody != null) {
            this.methodBody.addInstruction(instruction, comment);
        } else {
            program.addInstruction(instruction, comment);
        }
    }
    
    public void addErrorHeapOverflow() {
        errorManager.addErrorHeapOverflow(this);
    }
    
    public void addErrorStackOverflow(IMAProgram program, int maxStackSize) {
        errorManager.addErrorStackOverflow(this, program, maxStackSize);
    }
    
    public void addErrorOverflowFloat() {
        errorManager.addErrorOverflowFloat(this);
    }
    
    public void addErrorDivZeroInt() {
        errorManager.addErrorDivZeroInt(this);
    }
    
    public void addErrorInvalidTypeConversion() {
        errorManager.addErrorInvalidTypeConversion(this);
    }
    
    public void addErrorInvalidInput() {
        errorManager.addErrorInvalidInput(this);
    }

    public void addErrorNullDereference(int registerNumber) {
        errorManager.addErrorNullDereference(this,registerNumber);
    }
    
    public void addErrorNoReturn(String methodName) {
        errorManager.addErrorNoReturn(this, methodName);
    }
    
    /**
     * @see
     * fr.ensimag.ima.pseudocode.IMAProgram#display()
     */
    public String displayIMAProgram() {
        return program.display();
    }

    private final CompilerOptions compilerOptions;
    private final File source;
    /**
     * The main program. Every instruction generated will eventually end up here.
     */
    private final IMAProgram program = new IMAProgram();


    /**
     * Run the compiler (parse source file, generate code)
     *
     * @return true on error
     */
    public boolean compile() {
        String sourceFile = source.getAbsolutePath();
        String destFile = sourceFile.replace(".deca", ".ass");
        PrintStream err = System.err;
        PrintStream out = System.out;
        LOG.debug("Compiling file " + sourceFile + " to assembly file " + destFile);
        try {
            return doCompile(sourceFile, destFile, out, err);
        } catch (LocationException e) {
            e.display(err);
            return true;
        } catch (DecacFatalError e) {
            err.println(e.getMessage());
            return true;
        } catch (StackOverflowError e) {
            LOG.debug("stack overflow", e);
            err.println("Stack overflow while compiling file " + sourceFile + ".");
            return true;
        } catch (Exception e) {
            LOG.fatal("Exception raised while compiling file " + sourceFile
                    + ":", e);
            err.println("Internal compiler error while compiling file " + sourceFile + ", sorry.");
            return true;
        } catch (AssertionError e) {
            LOG.fatal("Assertion failed while compiling file " + sourceFile
                    + ":", e);
            err.println("Internal compiler error while compiling file " + sourceFile + ", sorry.");
            return true;
        }
    }

    /**
     * Internal function that does the job of compiling (i.e. calling lexer,
     * verification and code generation).
     *
     * @param sourceName name of the source (deca) file
     * @param destName name of the destination (assembly) file
     * @param out stream to use for standard output (output of decac -p)
     * @param err stream to use to display compilation errors
     *
     * @return true on error
     */
    private boolean doCompile(String sourceName, String destName,
            PrintStream out, PrintStream err)
            throws DecacFatalError, LocationException {
        AbstractProgram prog = doLexingAndParsing(sourceName, err);

        if (prog == null) {
            LOG.info("Parsing failed");
            return true;
        }
        assert(prog.checkAllLocations());
        if (this.getCompilerOptions().getParse()) {
            prog.decompile(out);
            return false;
        }
        
        prog.verifyProgram(this);
        assert(prog.checkAllDecorations());
        
        if (this.getCompilerOptions().getVerification()) {
            return false;
        }

        addComment("start main program");
        prog.codeGenProgram(this);
        addComment("end main program");
        LOG.debug("Generated assembly code:" + nl + program.display());
        LOG.info("Output file assembly file is: " + destName);

        FileOutputStream fstream = null;
        try {
            fstream = new FileOutputStream(destName);
        } catch (FileNotFoundException e) {
            throw new DecacFatalError("Failed to open output file: " + e.getLocalizedMessage());
        }

        LOG.info("Writing assembler file ...");

        program.display(new PrintStream(fstream));
        LOG.info("Compilation of " + sourceName + " successful.");
        return false;
    }

    /**
     * Build and call the lexer and parser to build the primitive abstract
     * syntax tree.
     *
     * @param sourceName Name of the file to parse
     * @param err Stream to send error messages to
     * @return the abstract syntax tree
     * @throws DecacFatalError When an error prevented opening the source file
     * @throws DecacInternalError When an inconsistency was detected in the
     * compiler.
     * @throws LocationException When a compilation error (incorrect program)
     * occurs.
     */
    protected AbstractProgram doLexingAndParsing(String sourceName, PrintStream err)
            throws DecacFatalError, DecacInternalError {
        DecaLexer lex;
        try {
            lex = new DecaLexer(CharStreams.fromFileName(sourceName));
        } catch (IOException ex) {
            throw new DecacFatalError("Failed to open input file: " + ex.getLocalizedMessage());
        }
        lex.setDecacCompiler(this);
        CommonTokenStream tokens = new CommonTokenStream(lex);
        DecaParser parser = new DecaParser(tokens);
        parser.setDecacCompiler(this);
        return parser.parseProgramAndManageErrors(err);
    }
    
}
