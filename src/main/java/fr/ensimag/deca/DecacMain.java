package fr.ensimag.deca;

import java.io.File;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;

/**
 * Main class for the command-line Deca compiler.
 *
 * @author gl38
 * @date 01/01/2020
 */
public class DecacMain {

    private static final Logger LOG = Logger.getLogger(DecacMain.class);

    public static void main(String[] args) {
        // example log4j message.
        LOG.info("Decac compiler started");
        boolean error = false;
        final CompilerOptions options = new CompilerOptions();
        try {
            options.parseArgs(args);
        } catch (CLIException e) {
            System.err.println("Error during option parsing:\n"
                    + e.getMessage());
            options.displayUsage();
            System.exit(1);
        }
        if (options.getPrintBanner()) {
            System.out.println("Equipe gl38 : \n"
                    + "\t - Anne Gaisné\n"
                    + "\t - Thomas Foulon\n"
                    + "\t - Martin Schneider\n"
                    + "\t - Wilfried Broissart\n"
                    + "\t - Rafael David Carrera Rodriguez");
            System.exit(0);
        }
        if (options.getSourceFiles().isEmpty()) {
            options.displayUsage();
            System.out.println("-b\t (banner)\t: affiche une bannière indiquant le nom de l'équipe\n"
                    + "-p\t (parse)\t: arrête decac après l’étape de construction de\n"
                    + "\t\t\t l’arbre, et affiche la décompilation de ce dernier\n"
                    + "\t\t\t (i.e. s’il n’y a qu’un fichier source à\n"
                    + "\t\t\t compiler, la sortie doit être un programme\n"
                    + "\t\t\t deca syntaxiquement correct)\n"
                    + "-v\t (verification)\t: arrête decac après l’étape de vérifications\n"
                    + "\t\t\t (ne produit aucune sortie en l’absence d’erreur)\n"
                    + ". -n\t (no check)\t: supprime les tests de débordement à l’exécution\n"
                    + "\t\t\t\t - débordement arithmétique lors des calculs flottants\n"
                    + "\t\t\t\t   (y compris lors d’une division par 0.0.)\n"
                    + "\t\t\t\t - débordement mémoire (pile ou tas)\n"
                    + "\t\t\t\t - déréférencement de null\n"
                    + "-r X\t (registers)\t: limite les registres banalisés disponibles à\n"
                    + "\t\t\t R0 ... R{X-1}, avec 4 <= X <= 16\n"
                    + "-d\t (debug)\t: active les traces de debug. Répéter l’option\n"
                    + "\t\t\t plusieurs fois pour avoir plus de traces. (3 fois maximum)\n"
                    + "-P\t (parallel)\t: s’il y a plusieurs fichiers sources,\n"
                    + "\t\t\t lance la compilation des fichiers en\n"
                    + "\t\t\t parallèle (pour accélérer la compilation)");
        }
        if (options.getParallel()) {
            
            ExecutorService executor = Executors.newFixedThreadPool(options.getSourceFiles().size());
            CompletionService<Boolean> completionService = new ExecutorCompletionService<>(executor);
            
            for (File source : options.getSourceFiles()) {
                completionService.submit(() -> {
                    DecacCompiler compiler = new DecacCompiler(options, source);
                    return compiler.compile();
                });
            }
            
            for (int terminations = 0; terminations<options.getSourceFiles().size() ; terminations++){
                try {
                    Future<Boolean> futureError = completionService.take();
                    if(futureError.get()){
                        error = true;
                    }
                } catch (InterruptedException ex) {
                    System.err.println(ex + ": Compilation interrupted");
                } catch (ExecutionException ex) {
                    System.err.println(ex + ": Error getting error code");
                }
            }
        } else {
            for (File source : options.getSourceFiles()) {
                DecacCompiler compiler = new DecacCompiler(options, source);
                if (compiler.compile()) {
                    error = true;
                }
            }
        }
        System.exit(error ? 1 : 0);
    }
}
