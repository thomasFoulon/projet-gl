/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.syntax;

import org.antlr.v4.runtime.Token;

/**
 *  Invalid float recognition error
 * @author gl38
 */
public class InvalidFloatException extends DecaRecognitionException {
    private String message;

    public InvalidFloatException(DecaParser recognizer, Token offendingToken, String message) {
        super(recognizer, offendingToken);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
