/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.syntax;

import org.antlr.v4.runtime.Token;

/**
 * Invalid int parsing with message "the integer cannot be parsed"
 * @author gaisnea
 */
public class InvalidIntParsing extends DecaRecognitionException {

    public InvalidIntParsing(DecaParser recognizer, Token offendingToken) {
        super(recognizer, offendingToken);
    }

    @Override
    public String getMessage() {
        return "the integer cannot be parsed";
    }
    
}
