/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.syntax;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 *  Exception when too many expressions are nested
 * @author gaisnea
 */
public class TooManyNestedException extends DecaRecognitionException {
    
    public TooManyNestedException(DecaParser recognizer, ParserRuleContext ctx) {
        super(recognizer, ctx);
    }

    @Override
    public String getMessage() {
        return "Too many nested expression";
    }
}
