#! /bin/sh

# Autor : gl38
# Initial version: 01/01/2020

# Automatically test the lexer on deca files in subdirectory syntax

# Go to root directory of project
cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:"$PATH"

test_lex_invalide () {
    if test_lex "$1" 2>&1 | grep -q -e "$1:[0-9][0-9]*:"
    then
        echo -e "\e[32mExpected error of test_lex on $1\e[0m"
    else
        echo -e "\e[31mUnexpected success of test_lex on $1\e[0m"
        exit 1
    fi
}

test_lex_valide() {
    if test_lex "$1" 2>&1 > new | grep -q -e "$1:[0-9][0-9]*:"
    then
        rm new
        echo -e "\e[31mUnexpected error of test_lex on $1\e[0m"
        exit 1
    else
        # Getting the .lis file that show the expected tree for the .deca given
        old_file=${1/valid\//valid\/expected_tokens\/}
        old_file=${old_file/.deca/.lis}
        # ne pas prendre en compte les lignes avec DEBUG ou INFO pour éviter le problème
        if ! diff -q new ${old_file} -I '[.*(DEBUG|INFO).*]'
        then
            rm new
            echo -e "\e[31mError on Token sequence of $1 : different from expected\e[0m"
            exit 1
        else
            rm new
            echo -e "\e[32mExpected success of test_lex on $1\e[0m"
        fi
    fi

}

test_lex_invalide ./src/test/deca/syntax/invalid/circular_include.deca
test_lex_invalide ./src/test/deca/syntax/invalid/include_not_found.deca
test_lex_invalide ./src/test/deca/syntax/invalid/incomplete_multiline_string.deca
test_lex_invalide ./src/test/deca/syntax/invalid/not_recognized_token_on_code.deca
test_lex_invalide ./src/test/deca/syntax/invalid/special_character.deca

find ./src/test/deca/syntax/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_lex_valide "$test_valide"
done
if [ $? -ne 0 ]; then
  exit 1
fi
