#! /bin/sh

# Author : gl38
# Initial version : 23/01/2020

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:./src/main/bin:"$PATH"

test_limit_register_valide() {
    file_ass=${1/.deca/.ass}
    rm -f "$file_ass" 2> /dev/null
    decac "$1" -r 4 || exit 1
    if [ ! -f "$file_ass" ]; then
        rm -f "$file_ass" 2> /dev/null
        echo -e "\e[31mUnexpected error : File .ass not generated for $1\e[0m"
        exit 1
    else
        results=$(ima "$file_ass") || exit 1
        expected=${1/valid\//valid\/expected_res\/}
        expected=${expected/.deca/.res}
        expected=$(cat "$expected") || exit 1
        if [ "$results" = "$expected" ]; then
            if grep "$file_ass" -e "R[4-9]" -e "R1[0-5]"; then
                rm -f "$file_ass" 2> /dev/null
                echo -e "\e[31mUnexpected error : R4 or higher used when -r 4 was specified in $file_ass\e[0m"
                exit 1
            else
                rm -f "$file_ass" 2> /dev/null
                echo -e "\e[32mExpected success of decac with -r 4 : no use of R4 or higher register on $1\e[0m"
            fi
        else
            echo "Unexpected error : wrong results for ima on $1"
            echo "$results"
            exit 1
        fi
    fi
}

find ./src/test/deca/codegen/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_limit_register_valide "$test_valide"
done