#! /bin/sh

# Autor : gl38
# Initial version : 10/01/2020

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:./src/main/bin:"$PATH"

test_code_gen_valide() {
    file_ass=${1/.deca/.ass}
    rm -f "$file_ass" 2> /dev/null
    decac "$1" || exit 1
    if [ ! -f "$file_ass" ]; then
        rm -f "$file_ass" 2> /dev/null
        echo -e "\e[31mUnexpected error : File .ass not generated for $1\e[0m"
        exit 1
    else
        results=$(ima "$file_ass")
        expected=${1/valid\//valid\/expected_res\/}
        expected=${expected/.deca/.res}
        expected=$(cat "$expected")
        rm -f "$file_ass" 2> /dev/null
        if [ "$results" = "$expected" ]; then
            echo -e "\e[32mExpected success of decac on $1 : ima results as expected file $file_ass\e[0m"
        else
            echo -e "\e[31mUnexpected error : wrong results for ima on $1\e[0m"
            echo "$results"
            exit 1
        fi
    fi
}

test_code_gen_invalide() {
    file_ass=${1/.deca/.ass}
    rm -f "$file_ass" 2> /dev/null
    decac "$1" || exit 1
    if [ ! -f "$file_ass" ]; then
        rm -f "$file_ass" 2> /dev/null
        echo -e "\e[31mUnexpected error : File .ass not generated for $1\e[0m"
        exit 1
    else
        results=$(ima "$file_ass" &1>/dev/null)
        expected=${1/invalid\//invalid\/expected_res\/}
        expected=${expected/.deca/.res}
        expected=$(cat "$expected")
        rm -f "$file_ass" 2> /dev/null
        if [ "$results" = "$expected" ]; then
            echo -e "\e[32mExpected success of decac on $1 : ima results as expected file $file_ass\e[0m"
        else
            echo -e "\e[31mUnexpected error : wrong results for ima on $1\e[0m"
            echo "$results"
            exit 1
        fi
    fi
}

find ./src/test/deca/codegen/invalid/ -name "*.deca" -print0 | while read -d $'\0' test_invalide
do
    test_code_gen_invalide "$test_invalide" || exit 1
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/codegen/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_code_gen_valide "$test_valide" || exit 1
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/codegen/interactive/ -name "*.deca" -print0 | while read -d $'\0' test_interactif
do
    file_ass=${test_interactif/.deca/.ass}
    rm -f "$file_ass" 2> /dev/null
    decac "$test_interactif"
    if [ ! -f "$file_ass" ]; then
        rm -f "$file_ass" 2> /dev/null
        echo -e "\e[31mUnexpected error : File .ass not generated for $test_interactif\e[0m"
        exit 1
    else
        echo -e "\e[32mExpected success of decac on $1 : file generated at $file_ass\e[0m"
    fi
    rm -f "$file_ass" 2> /dev/null
done
if [ $? -ne 0 ]; then
  exit 1
fi