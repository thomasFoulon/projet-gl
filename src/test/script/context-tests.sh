#! /bin/sh

# Autor : gl38
# Initial version: 10/01/2020

# Automatically test the lexer on deca files in subdirectory syntax

# Go to root directory of project
cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:"$PATH"

test_context_invalide () {
    if test_context "$1" 2>&1 | grep -q -e "$1:[0-9][0-9]*:"
    then
        echo -e "\e[32mExpected error of test_context on $1\e[0m"
    else
        echo -e "\e[31mUnexpected success of test_context on $1\e[0m"
        exit 1
    fi
}

test_context_valide() {
    if test_context "$1" 2>&1 > new | grep -q -e "$1:[0-9][0-9]*:"
    then
        rm new
        echo -e "\e[31mUnexpected error of test_context on $1\e[0m"
        exit 1
    else
        # Getting the .lis file that show the expected tree for the .deca given
        old_file=${1/valid\//valid\/expected_context_tree\/}
        old_file=${old_file/.deca/.lis}
        if ! diff -q new ${old_file} -I '[.*(DEBUG|INFO).*]'
        then
            rm new
            echo -e "\e[31mError on Abstract Context tree of $1 : different from expected\e[0m"
            exit 1
        else
            rm new
            echo -e "\e[32mExpected success of test_context on $1\e[0m"
        fi
    fi

}

find ./src/test/deca/context/invalid/ -name "*.deca" -print0 | while read -d $'\0' test_invalide
do
    test_context_invalide "$test_invalide"
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/context/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_context_valide "$test_valide"
done
if [ $? -ne 0 ]; then
  exit 1
fi