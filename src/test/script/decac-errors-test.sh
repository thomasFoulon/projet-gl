#! /bin/sh

# Autor : gl38
# Initial version : 27/01/2020
# Test de decac pour cas de erreurs (fichier illisible ou non trouvable)

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:./src/main/bin:"$PATH"

#To test with non readable input files (as it's not possible to add them to git)
#decac ./src/test/deca/notreadable.deca

#if [ $? -eq 0 ]; then
#    echo -e "\e[31mError not thrown on not readable file\e[0m"
#    exit 1
#else 
#    echo -e "\e[32mError thrown succesfully on not readable file\e[0m"  
#fi

decac ./src/imaginaryfolder/imaginaryfile.deca

if [ $? -eq 0 ]; then
    echo -e "\e[31mError not thrown on not findable file\e[0m"
    exit 1
else 
    echo -e "\e[32mError thrown succesfully on not findable file\e[0m"  
fi

#To test with non readable output files (as it's not possible to add them to git)
#decac ./src/test/deca/empty_main_not_readable_ass.deca

#if [ $? -eq 0 ]; then
#    echo -e "\e[31mError not thrown on not readable output file\e[0m"
#    exit 1
#else 
#    echo -e "\e[32mError thrown succesfully on not readable output file\e[0m"  
#fi