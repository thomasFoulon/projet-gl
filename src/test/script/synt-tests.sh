#! /bin/sh

# Autor : gl38
# Version initiale : 01/01/2020

# Script to automatically test the parser

# Go to root directory of project
cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:"$PATH"

test_synt_invalide () {
    if test_synt "$1" 2>&1 | grep -q -e "$1:[0-9][0-9]*:"
    then
        echo -e "\e[32mExpected error of test_synt on $1\e[0m"
    else
        echo -e "\e[31mUnexpected success of test_synt on $1\e[0m"
        exit 1
    fi
}

test_synt_valide() {
    if ! test_synt "$1" > new 2> /dev/null
    then
        rm new
        echo -e "\e[31mUnexpected error of test_synt on $1\e[0m"
        exit 1
    else
        # Getting the .lis file that show the expected tree for the .deca given
        old_file=${1/valid\//valid\/expected_tree\/}
        old_file=${old_file/.deca/.lis}
        if ! diff new ${old_file} -I '[.*(DEBUG|INFO).*]'
        then
            rm new
            echo -e "\e[31mError on Abstract tree of $1 : different from expected\e[0m"
            exit 1
        else
            rm new
            echo -e "\e[32mExpected success of test_synt on $1\e[0m"
        fi
    fi

}

find ./src/test/deca/syntax/invalid/ -name "*.deca" -print0 | while read -d $'\0' test_invalide
do
    test_synt_invalide "$test_invalide"
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/syntax/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_synt_valide "$test_valide"
done
if [ $? -ne 0 ]; then
  exit 1
fi