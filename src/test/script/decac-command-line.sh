#! /bin/sh

# Test of command line interface for decac

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/main/bin:"$PATH"

#Test decac without argument
decac_no_arg=$(decac)
if [ "$?" -ne 0 ]; then
    echo -e "\e[31mERROR : decac without arguments ended with non zero status\e[0m"
    exit 1
fi
if [ "$decac_no_arg" = "" ]; then
    echo -e "\e[31mERROR: decac without arguments produced no output.\e[0m"
    exit 1
fi 

# Test decac -b
decac_b_invalid=$(decac -b -p 2> /dev/null)
if [ "$?" -eq 0 ]; then
    echo -e "\e[31mERROR : decac -b -p worked unexpectedly.\e[0m"
    exit 1
fi

decac_b=$(decac -b)
if [ "$?" -ne 0 ]; then
    echo -e "\e[31mERROR: decac -b ended with non zero status.\e[0m"
    exit 1
fi
if [ "$decac_b" = "" ]; then
    echo -e "\e[31mERROR: decac -b produced no output.\e[0m"
    exit 1
fi
if echo "$decac_b" | grep -i -e "erreur" -e "error"; then
    echo -e "\e[31mERROR: output of decac -b contains error or erreur.\e[0m"
    exit 1
fi

echo -e "\e[32mNo problem detected on decac -b\e[0m"

# Test decac -p
decac_p_invalid=$(decac -p -v 2> /dev/null)
if [ "$?" -eq 0 ]; then
    echo -e "\e[31mERROR: decac -p -v worked unexpectedly\e[0m"
    exit 1
fi

echo -e "\e[32mNo problem detected on decac -p\e[0m"

# Test decac -r
if ! decac -r 3 2>&1 | grep -i -e "integer between 4 and 16 given : 3" > /dev/null; then
    echo -e "\e[31mERROR: decac -r 3 worked unexpectedly\e[0m"
    exit 1
fi
if ! decac -r 17 2>&1 | grep -i -e "integer between 4 and 16 given : 17" > /dev/null; then
    echo -e "\e[31mERROR: decac -r 17 worked unexpectedly\e[0m"
    exit 1
fi
if ! decac -r test 2>&1 | grep -i -e "must be an integer given : test" > /dev/null; then
    echo -e "\e[31mERROR: decac -r test worked unexpectedly\e[0m"
    exit 1
fi
echo -e "\e[32mNo problem detected on decac -r\e[0m"

# Test with no matching file
if ! decac test 2>&1 | grep -i -e "unrecognized argument" > /dev/null; then
    echo -e "\e[31mERROR: decac test worked unexpectedly\e[0m"
    exit 1
fi
if ! decac test.deca 2>&1 | grep -i -e "no such file exists" > /dev/null; then
    echo -e "\e[31mERROR: decac test.deca worked unexpectedly\e[0m"
    exit 1
fi
echo -e "\e[32mNo problem detected on decac with no matching file\e[0m"