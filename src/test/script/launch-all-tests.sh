#! /bin/sh

# Autor : gl38
# Initial version: 16/01/2020

# Automatically launch all test (for cobertura report)

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script:"$PATH"

lex-tests.sh || exit 1
synt-tests.sh || exit 1
context-tests.sh || exit 1
codegen-tests.sh || exit 1
#limit-register-test.sh || exit 1
basic-lex.sh || exit 1
basic-synt.sh || exit 1
basic-context.sh || exit 1
basic-gencode.sh || exit 1
basic-decac.sh || exit 1
decac-command-line.sh || exit 1
decac-parsing-options.sh || exit 1
decac-verification-option.sh || exit 1
decac-errors-test.sh || exit 1
decac-parallel-test.sh || exit 1