#! /bin/sh

# Autor : gl38
# Initial version : 16/01/2020
# Test de decac -p sur les cas de tests invalides pour syntax et pour les cas valides de codegen, context et syntax

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:./src/main/bin:"$PATH"

test_decac_p_valide() {
    decac -p "$1" | grep -v -e "INFO" -e "DEBUG" > tmp.deca
    if [ ! -f tmp.deca ]; then
        echo -e "\e[31mUnexpected error : decompilation not done for $1\e[0m"
        exit 1
    else
        if test_synt tmp.deca 2> /dev/null 1> /dev/null
        then
            echo -e "\e[32mExpected success of decac -p on $1\e[0m"
        else
            rm tmp.deca 2> /dev/null
            echo -e "\e[31mUnexpected error on verification of decompilation on $1\e[0m"
            exit 1
        fi
    fi
    rm -f tmp.deca 2> /dev/null
}

test_decac_p_invalide() {
    if  decac -p "$1" > tmp.deca 2> /dev/null; then
        echo -e "\e[31mUnexpected success on decompilation on $1\e[0m"
        exit 1
    else
        echo -e "\e[32mExpected error of decac -p on $1\e[0m"
    fi
    rm -f tmp.deca 2> /dev/null
}

find ./src/test/deca/syntax/invalid/ -name "*.deca" -print0 | while read -d $'\0' test_invalide
do
    test_decac_p_invalide "$test_invalide"
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/*/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_decac_p_valide "$test_valide"
done
if [ $? -ne 0 ]; then
  exit 1
fi