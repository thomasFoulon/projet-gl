#! /bin/sh

# Autor : gl38
# Initial version : 20/01/2020
# Test de decac -v sur les cas de tests invalides pour syntax et syntaxe contextuelle et pour les cas valides de codegen, context et syntaxe contextuelle

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:./src/main/bin:"$PATH"

test_decac_v_valide() {
    if ! decac -v "$1" 2> /dev/null; then
        echo -e "\e[31mUnexpected error : contextual verification failed for $1\e[0m"
        exit 1
    else
        echo -e "\e[32mExpected success of decac -v on $1\e[0m"
    fi
}

test_decac_v_invalide() {
    if  decac -v "$1" 2> /dev/null; then
        echo -e "\e[31mUnexpected success on contextual verification on $1\e[0m"
        exit 1
    else
        echo -e "\e[32mExpected error of decac -v on $1\e[0m"
    fi
    rm -f tmp.deca 2> /dev/null
}

find ./src/test/deca/syntax/invalid/ -name "*.deca" -print0 | while read -d $'\0' test_invalide
do
    test_decac_v_invalide "$test_invalide"
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/context/invalid/ -name "*.deca" -print0 | while read -d $'\0' test_invalide
do
    test_decac_v_invalide "$test_invalide"
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/context/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_decac_v_valide "$test_valide"
done
if [ $? -ne 0 ]; then
  exit 1
fi

find ./src/test/deca/codegen/valid/ -name "*.deca" -print0 | while read -d $'\0' test_valide
do
    test_decac_v_valide "$test_valide"
done
if [ $? -ne 0 ]; then
  exit 1
fi