#! /bin/sh

# Autor : gl38
# Initial version : 27/01/2020
# Test de decac -P sur tous les cas de tests valides

cd "$(dirname "$0")"/../../.. || exit 1

PATH=./src/test/script/launchers:./src/main/bin:"$PATH"

test_parallel_valide(){
    rm ./src/test/deca/codegen/valid/*.ass 2> /dev/null
    decac -P ./src/test/deca/codegen/valid/*.deca 

    if [ $? -ne 0 ]; then
      echo -e "\e[31mParallel compilation unsuccessful on valid codegen\e[0m"
      exit 1
    else 
      echo -e "\e[32mParallel compilation successful on valid codegen. Testing results...\e[0m"  
    fi

    find ./src/test/deca/codegen/valid/ -name "*.ass" -print0 | while read -d $'\0' file_ass
    do
        file_deca=${file_ass/.ass/.deca}
        results=$(ima "$file_ass")
        expected=${file_ass/valid\//valid\/expected_res\/}
        expected=${expected/.ass/.res}
        expected=$(cat "$expected")
        if [ "$results" = "$expected" ]; then
            echo -e "\e[32mExpected success of decac -P on $file_deca : ima results as expected file $file_ass\e[0m"
        else
            echo -e "\e[31mUnexpected error : wrong results for ima on $file_deca\e[0m"
            echo "$results"
            rm ./src/test/deca/codegen/valid/*.ass 2> /dev/null
            exit 1
        fi
    done
    if [ $? -ne 0 ]; then
        exit 1
    fi
    rm ./src/test/deca/codegen/valid/*.ass 2> /dev/null
}

test_parallel_invalide(){
    rm ./src/test/deca/codegen/invalid/*.ass 2> /dev/null
    decac -P ./src/test/deca/codegen/invalid/*.deca 

    if [ $? -ne 0 ]; then
      echo -e "\e[31mParallel compilation unsuccessful on invalid codegen\e[0m"
      exit 1
    else 
      echo -e "\e[32mParallel compilation successful on invalid codegen. Testing results...\e[0m"  
    fi

    find ./src/test/deca/codegen/invalid/ -name "*.ass" -print0 | while read -d $'\0' file_ass
    do
        file_deca=${file_ass/.ass/.deca}
        results=$(ima "$file_ass")
        expected=${file_ass/invalid\//invalid\/expected_res\/}
        expected=${expected/.ass/.res}
        expected=$(cat "$expected")
        if [ "$results" = "$expected" ]; then
            echo -e "\e[32mExpected success of decac -P on $file_deca : ima results as expected file $file_ass\e[0m"
        else
            echo -e "\e[31mUnexpected error : wrong results for ima on $file_deca\e[0m"
            echo "$results"
            rm ./src/test/deca/codegen/invalid/*.ass 2> /dev/null
            exit 1
        fi
    done
    if [ $? -ne 0 ]; then
        exit 1
    fi
    rm ./src/test/deca/codegen/invalid/*.ass 2> /dev/null
}

test_parallel_valide
test_parallel_invalide
