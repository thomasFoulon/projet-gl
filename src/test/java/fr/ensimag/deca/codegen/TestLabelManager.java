/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.codegen;

import fr.ensimag.ima.pseudocode.Label;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test for the LabelManager class.
 *
 * @author gl38
 * @date 08/01/2020
 */
public class TestLabelManager {
    
    @Test
    public void testNewIfLabels() {
        LabelManager l = new LabelManager();
        Label[] ifLabels = l.newIfLabels();
        Label ifEndExpected = new Label("L_EndIf.1");
        Label ifElseExpected = new Label("L_Else.1");
        assertEquals(ifLabels.length, 2);
        assertEquals(ifLabels[0].toString(), ifEndExpected.toString());
        assertEquals(ifLabels[1].toString(), ifElseExpected.toString());
    }
    
    @Test
    public void testNewWhileLabels() {
        LabelManager l = new LabelManager();
        Label[] whileLabels = l.newWhileLabels();
        Label whileStartExpected = new Label("L_WhileStart.1");
        Label whileCondExpected = new Label("L_WhileCond.1");
        assertEquals(whileLabels.length, 2);
        assertEquals(whileLabels[0].toString(), whileStartExpected.toString());
        assertEquals(whileLabels[1].toString(), whileCondExpected.toString());
    }
    
    @Test
    public void testNewInstanceOfLabels() {
        LabelManager l = new LabelManager();
        Label[] instOfLabel = l.newInstanceOfLabels();
        Label start = new Label("L_InstanceOfStart.1");
        Label end = new Label("L_InstanceOfEnd.1");
        Label trueL = new Label("L_InstanceOfTrue.1");
        Label falseL = new Label("L_InstanceOfFalse.1");
        assertEquals(instOfLabel.length, 4);
        assertEquals(instOfLabel[0].toString(), start.toString());
        assertEquals(instOfLabel[1].toString(), end.toString());
        assertEquals(instOfLabel[2].toString(), trueL.toString());
        assertEquals(instOfLabel[3].toString(), falseL.toString());
    }
    
    @Test
    public void testNewCastOKLabel() {
        LabelManager l = new LabelManager();
        Label castLabel = l.newCastOKLabel();
        Label castLabelExpected = new Label("L_CastOK.1");
        assertEquals(castLabel.toString(), castLabelExpected.toString());
    }
    
    @Test
    public void testNewEndBooleanExpLabel() {
        LabelManager l = new LabelManager();
        Label endExpLabel = l.newEndBooleanExpLabel();
        Label endExpLabelExpected = new Label("L_EndBoolExp.1");
        assertEquals(endExpLabel.toString(), endExpLabelExpected.toString());
    }
    
    @Test
    public void testNewBooleanDeclVarLabels() {
        LabelManager l = new LabelManager();
        Label[] boolDeclLabel = l.newBooleanDeclVarLabels();
        Label trueL = new Label("L_DefTrue.1");
        Label falseL = new Label("L_DefFalse.1");
        Label end = new Label("L_EndDefBool.1");
        assertEquals(boolDeclLabel.length, 3);
        assertEquals(boolDeclLabel[0].toString(), trueL.toString());
        assertEquals(boolDeclLabel[1].toString(), falseL.toString());
        assertEquals(boolDeclLabel[2].toString(), end.toString());
    }
}
