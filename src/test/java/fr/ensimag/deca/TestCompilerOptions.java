/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca;

import static fr.ensimag.deca.CompilerOptions.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test of the CompilerOptions class.
 * @author gl38
 */
public class TestCompilerOptions {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    
    @Test
    public void testCompilerOptionsWithNoArgument() throws CLIException {
        String []args = {};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertEquals(options.getDebug(), QUIET);
    }
    
    @Test
    public void testCompilerOptionsBanner() throws CLIException {
        String []args = {"-b"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertTrue(options.getPrintBanner());
    }
    
    @Test
    public void testCompilerOptionsBannerWrong() throws CLIException {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("-b needs to be the only argument");
        String []args = {"-b", "-p"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertTrue(options.getPrintBanner());
    }
    
    @Test
    public void testCompilerOptionsParse() throws CLIException {
        String []args = {"-p"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertTrue(options.getParse());
    }
    
    @Test
    public void testCompilerOptionsParallel() throws CLIException {
        String []args = {"-P"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertTrue(options.getParallel());
    }
    
    @Test
    public void testCompilerOptionsVerification() throws CLIException {
        String []args = {"-v"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertTrue(options.getVerification());
    }
    
    @Test
    public void testCompilerOptionsNoCheck() throws CLIException {
        String []args = {"-n"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertTrue(options.getNoCheck());
    }
    
    @Test
    public void testCompilerOptionsDebug() throws CLIException {        
        String []argsInfo = {"-d"};
        CompilerOptions optionsInfo = new CompilerOptions();
        optionsInfo.parseArgs(argsInfo);
        assertEquals(optionsInfo.getDebug(), INFO);
        
        String []argsDebug = {"-d", "-d"};
        CompilerOptions optionsDebug = new CompilerOptions();
        optionsDebug.parseArgs(argsDebug);
        assertEquals(optionsDebug.getDebug(), DEBUG);
        
        String []argsTrace = {"-d", "-d", "-d"};
        CompilerOptions optionsTrace = new CompilerOptions();
        optionsTrace.parseArgs(argsTrace);
        assertEquals(optionsTrace.getDebug(), TRACE);
        
        String []argsAll = {"-d", "-d", "-d", "-d"};
        CompilerOptions optionsAll = new CompilerOptions();
        optionsAll.parseArgs(argsAll);
        assertEquals(optionsAll.getDebug(), 4);
    }
    
    @Test
    public void testCompilerOptionsRegister() throws CLIException {
        String [] args = {"-r", "5"};
        CompilerOptions optionsRight = new CompilerOptions();
        optionsRight.parseArgs(args);
        assertEquals(optionsRight.getRegisters(), 4);        
    }
    
    @Test
    public void testCompilerOptionsRegisterTooHigh() throws Exception {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("register number must be an integer between 4 and 16 given : 17");
        String [] argsTooHigh = {"-r", "17"};
        CompilerOptions optionsTooHigh = new CompilerOptions();
        optionsTooHigh.parseArgs(argsTooHigh);
    }
    
    @Test
    public void testCompilerOptionsRegisterTooLow() throws CLIException {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("register number must be an integer between 4 and 16 given : 3");
        String [] argsTooLow = {"-r", "3"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(argsTooLow);
    }
    
    @Test
    public void testCompilerOptionsRegisterNoInteger() throws CLIException {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("-r X : X must be an integer given : test");
        String [] args = {"-r", "test"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
    }
    
    @Test
    public void testCompilerOptionsVerifParse() throws CLIException {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("error on argument -v : -p already specified");
        String [] args = {"-p", "-v"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
    }
    
    @Test
    public void testCompilerOptionsParseVerif() throws CLIException {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("error on argument -p : -v already specified");
        String [] args = {"-v", "-p"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
    }
    
    @Test
    public void testCompilerOptionsFile() throws CLIException {
        String [] args = {"src/test/deca/syntax/valid/include_empty.deca", "src/test/deca/syntax/valid/include_empty.deca"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
        assertFalse(options.getSourceFiles().isEmpty());
        assertEquals(options.getSourceFiles().size(), 1);
    }
    
    @Test
    public void testCompilerOptionsUnrecognizedArg() throws CLIException {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("unrecognized argument : test");
        String [] args = {"test"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
    }
    
    @Test
    public void testCompilerOptionsNoSuchFile() throws CLIException {
        expectedEx.expect(CLIException.class);
        expectedEx.expectMessage("test.deca : no such file exists");
        String [] args = {"test.deca"};
        CompilerOptions options = new CompilerOptions();
        options.parseArgs(args);
    }
    
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
    
    @Test
    public void testCompilerOptionsDisplayUsage() {
        CompilerOptions options = new CompilerOptions();
        options.displayUsage();
        assertEquals("Usage : decac [[-p | -v] [-n] [-r X] [-d]* [-P] <fichier deca>...] | [-b]\n", outContent.toString());
    }
}
