/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.context;

import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.Location;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for the EnvironmentExp class.
 *
 * @author gl38
 * @date 08/01/2020
 */
public class TestEnvironmentExp {
    
    private SymbolTable typesT;
    private SymbolTable t;
    private SymbolTable.Symbol vX, vY, vZ;
    private Type intT, floatT;
    
    @Before
    public void setup(){
        // Types
        typesT = new SymbolTable();
        intT = new IntType(typesT.create("int"));
        floatT = new FloatType(typesT.create("float"));
        // Variables
        t = new SymbolTable();
        vX = t.create("x");
        vY = t.create("y");
        vZ = t.create("z");
    }
    
    @Test
    public void testEmptyEnvironmentExp(){
        EnvironmentExp e = new EnvironmentExp(null);
        assertNull(e.get(vX));
        assertNull(e.get(vX));
    }
    
    @Test
    public void testEnvironmentExpNoParent(){
        // Environment with no parent
        EnvironmentExp e = new EnvironmentExp(null);
        ExpDefinition vXDef = new VariableDefinition(intT, Location.BUILTIN);
        ExpDefinition vYDef = new VariableDefinition(floatT, Location.BUILTIN);
        try {
            e.declare(vX, vXDef);
            e.declare(vY, vYDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        assertEquals(vXDef, e.get(vX));
        assertEquals(vYDef, e.get(vY));
        assertNull(e.get(vZ));
    }
    
    @Test
    public void testEnvironmentExpWithParent(){
        // Parent environment
        EnvironmentExp parent = new EnvironmentExp(null);
        ExpDefinition vYDef = new VariableDefinition(floatT, Location.BUILTIN);
        try {
            parent.declare(vY, vYDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Child environment
        EnvironmentExp child = new EnvironmentExp(parent);
        ExpDefinition vXDef = new VariableDefinition(intT, Location.BUILTIN);
        ExpDefinition vZDef = new VariableDefinition(floatT, Location.BUILTIN);
        try {
            child.declare(vX, vXDef);
            child.declare(vZ, vZDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        assertEquals(vZDef, child.get(vZ));
        assertEquals(vXDef, child.get(vX));
        // Has to research in the parent environment
        assertEquals(vYDef, child.get(vY));
    }
    
    @Test
    public void testEnvironmentExpWithGrandParent(){
        // Grand parent environment
        EnvironmentExp grandParent = new EnvironmentExp(null);
        ExpDefinition vYDef = new VariableDefinition(floatT, Location.BUILTIN);
        try {
            grandParent.declare(vY, vYDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Parent environment
        EnvironmentExp parent = new EnvironmentExp(grandParent);
        ExpDefinition vZDef = new VariableDefinition(floatT, Location.BUILTIN);
        try {
            parent.declare(vZ, vZDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Child environment
        EnvironmentExp child = new EnvironmentExp(parent);
        ExpDefinition vXDef = new VariableDefinition(intT, Location.BUILTIN);
        try {
            child.declare(vX, vXDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Has to research in the grand parent environment
        assertEquals(vYDef, child.get(vY));
        // Has to research in the parent environment
        assertEquals(vZDef, child.get(vZ));
        // Has to research in its proper environment
        assertEquals(vXDef, child.get(vX));
    }
    
    @Test
    public void testStacking(){
        // Parent environment
        EnvironmentExp parent = new EnvironmentExp(null);
        ExpDefinition vYDef = new VariableDefinition(floatT, Location.BUILTIN);
        try {
            parent.declare(vY, vYDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Child environment
        EnvironmentExp child = new EnvironmentExp(parent);
        ExpDefinition vXDef = new VariableDefinition(intT, Location.BUILTIN);
        ExpDefinition vZDef = new VariableDefinition(floatT, Location.BUILTIN);
        try {
            child.declare(vX, vXDef);
            child.declare(vZ, vZDef);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // New parent environment
        EnvironmentExp parent2 = new EnvironmentExp(null);
        ExpDefinition vYDef2 = new VariableDefinition(intT, Location.BUILTIN); // y is an int in this environment and not a float
        try {
            parent2.declare(vY, vYDef2);
        } catch (EnvironmentExp.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentExp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Stacking (the child has this new parent)
        child.stacking(parent2);
        
        assertEquals(vZDef, child.get(vZ));
        assertEquals(vXDef, child.get(vX));
        // Has to research in the parent environment
        assertNotEquals(vYDef, child.get(vY));
        assertEquals(vYDef2, child.get(vY));
    }
    
}
