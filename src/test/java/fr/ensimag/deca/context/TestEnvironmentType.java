/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ensimag.deca.context;

import fr.ensimag.deca.CLIException;
import fr.ensimag.deca.context.EnvironmentType.DoubleDefException;
import fr.ensimag.deca.tools.SymbolTable;
import fr.ensimag.deca.tree.Location;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Test for the EnvironmentType class.
 *
 * @author gl38
 * @date 13/01/2020
 */
public class TestEnvironmentType {
    
    private SymbolTable typesT;
    private Type intT, floatT;
    
    @Before
    public void setup(){
        typesT = new SymbolTable();
        intT = new IntType(typesT.create("int"));
        floatT = new FloatType(typesT.create("float"));
    }
    
    @Test
    public void testEmptyEnvironmentType(){
        EnvironmentType e = new EnvironmentType();
        assertNull(e.get(typesT.create("int")));
        assertNull(e.get(typesT.create("float")));
    }
    
    @Test
    public void testEnvironmentType(){
        EnvironmentType e = new EnvironmentType();
        TypeDefinition intDef = new TypeDefinition(intT, Location.BUILTIN);
        TypeDefinition floatDef = new TypeDefinition(floatT, Location.BUILTIN);
        try {
            e.declare(typesT.create("int"), intDef);
            e.declare(typesT.create("float"), floatDef);
        } catch (EnvironmentType.DoubleDefException ex) {
            Logger.getLogger(TestEnvironmentType.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        assertEquals(intDef, e.get(typesT.create("int")));
        assertEquals(floatDef, e.get(typesT.create("float")));
        assertNull(e.get(typesT.create("string")));
    }
    
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    
    @Test
    public void testEnvironmentDoubleDefException() throws DoubleDefException {
        expectedEx.expect(DoubleDefException.class);
        EnvironmentType e = new EnvironmentType();
        TypeDefinition intDef = new TypeDefinition(intT, Location.BUILTIN);
        e.declare(typesT.create("int"), intDef);
        e.declare(typesT.create("int"), intDef);        
    }
    
}
